# # Investigate methods of calculating spectra
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import scipy as sp
from scipy import signal
from scipy.fftpack import fft, ifft, fftshift

def Do_spectral_plot_local(Record_time,freq,power,f_avg,Pden_avg,f2,Pden2):
###############################################################
   # Do time series plot
    plt.figure(figsize=(20, 8))
    plt.title(Record_time,fontsize=14)
    plt.xlabel('Frequency (Hz)',fontsize=14) # Display Frequency on X-axis
    plt.ylabel('Spectral density ($m^2$/Hz)',fontsize=16)
    plt.tick_params(axis='both', which='major', labelsize=15)
    plt.xticks(np.arange(0, max(freq)+1, 0.1))
    plt.grid(True)
    plt.xlim(left=0.0,right=0.6)
    plt.plot(freq,power,marker="o",linewidth=2,color='b',markeredgecolor='b',markerfacecolor='none',markersize=5,label='Datawell')
    plt.fill(freq,power,c='xkcd:light sky blue')
    plt.plot(f2,Pden2,linewidth=2,label="Welch's method")
    plt.plot(f_avg,Pden_avg,marker="o",linewidth=2,color='g',markeredgecolor='g',markerfacecolor='none',markersize=5,label='Band averaged method')
    plt.legend(loc='upper right',fontsize=16,frameon=False)
##    plt.tight_layout()
##    plt.show()

    return()    # Do_spectral_plot()

def Do_Fourier(Heave,fs,N,Ords,freq):
###############################################################  
    s = 0; t = N

    # generate arrays of zeros for initial PSD to be averaged
    PSD_m = np.zeros(int(N/2))

    # loop until 1600s (2048 samples) have been processed
    for k in range(8):

        # Get 256 heave values
        hk = Heave[s:t]

        # Get Tukey window (with Hann window for first and last 32 samples, and rectangular window between).
        wk1 = signal.tukey(256,alpha=64/256)

    # Normalise all window coefficients
        wk = wk1/np.sqrt(fs*np.sum(wk1**2))

    # Apply FFT to calculate fourier coefficients    
        Hl = fft(hk*np.asarray(wk))
        Cvv = Hl.real**2 + Hl.imag**2

    # Power spectral density obtained from the Fourier coefficients (where frequencies range from 0.0-0.64Hz in steps of 0.005Hz.)
        PSD = np.zeros(int(Ords))
        PSD[0] = Cvv[0]
        PSD[Ords-1] = Cvv[Ords-1]
        for i in range(1,Ords):
            PSD[i] = Cvv[i] + Cvv[N-i]

        PSD1 = PSD

    # Smooth all coefficients
        for i in range(2,Ords-1):
            PSD[i] = PSD1[i-1]/4 + PSD1[i]/2 + PSD1[i+1]/4 

    # add this calculated PSD to total
        PSD_m += PSD

    # move to next 200s (256 samples)
        s = t
        t = t + N
        
    return(PSD_m,Cvv)    # Do_Fourier()

def Do_Fourier_vectors(Heave,fs,N,Ords):
###############################################################    

    s = 0; t = N

    # generate arrays of zeros for initial PSD to be averaged
    alpha_f_sum = np.zeros(int(N))
    beta_f_sum = np.zeros(int(N))

    # loop until 1600s (2048 samples) have been processed
    for k in range(8):

        # Get 256 heave values
        hk = Heave[s:t]

        # Get Tukey window (with Hann window for first and last 32 samples, and rectangular window between).
        wk1 = signal.tukey(256,alpha=64/256)

    # Normalise all window coefficients
        wk = wk1/np.sqrt(fs*np.sum(wk1**2))

    # Apply FFT to calculate fourier coefficients    
        Hl = fft(hk*np.asarray(wk))
        alpha_f = Hl.real
        beta_f = Hl.imag

        alpha_f_sum += alpha_f
        beta_f_sum += beta_f

    # move to next 200s (256 samples)
        s = t
        t = t + N
        
    return(alpha_f_sum/8,beta_f_sum/8)    # Do_Fourier_vectors()
