# -*- coding: utf-8 -*-
"""HVA_tools

A series of subroutines used by Read_HVA

Author: Jim Waldron
Date: December 2019
"""

import array as arr
import binascii
#import colorama
import itertools
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import scipy as sp
import scipy.stats
#import statistics as s
import sys
#import tkinter as tk

#from colorama import Fore, Back, Style
from datetime import datetime, time, timedelta
from itertools import count # izip for maximum efficiency
from mpl_toolkits.mplot3d import Axes3D
from pathlib import Path, PureWindowsPath
from scipy import fftpack, signal
from scipy.signal import tukey
from scipy.stats import norm,rayleigh,skew,kurtosis
from statistics import mean
#from tkinter import filedialog
#from tkinter import *

def Do_CRC(packet):
##########################################
    """
    Do a CRC-4 checksum calculation on a Hexadecimal packet

    See DWTP 2.1.1 pp25-26.

    Args:
        packet (str): Hexadecimal string to be validated
        key_table (list): CRC checksum values.
        crc (str): 
        
    Returns:
        Nil.
   """    

    key_table = [0,3,6,5,12,15,10,9,11,8,13,14,7,4,1,2]; crc = 0
    for __ in range(len(packet)):
        crc = key_table[(crc^(int(packet[__][0],16))) & int('0x0f',16)]        # First nibble
        if (__ > 0): 
            crc = key_table[(crc^int(packet[__][1],16)) & int('0x0f',16)]      # Second nibble

    crc = (crc^int(packet[0][1],16)) & int('0x0f',16)
    if (crc != 0): 
        print('CRC Error!',crc)
    
    return   # Do_CRC()
	
def Do_Timestamp(packet):
##########################################
    """
    Decode Time message 

    See DWTP p. 27

    Args:
        packet (str): Hexadecimal string to be validated
        Datawell_start_time (DateTime): Datawell start time used in calculations - 1 January 1970
        Record_time (DateTime): The Date/Time of the packet
    Returns:
        Calculated time.
   """    
   
    Do_CRC(packet)
    Datawell_start_time = datetime(1970,1,1)
    i = int(packet[2]+packet[3]+packet[4]+packet[5],16)
    if i == 2**32-1:                   # Check for NaN
        print("Timestamp = NaN")
        Record_time = datetime.date.min
    else:
        Record_time = Datawell_start_time + timedelta(seconds=i) + timedelta(hours=10)  # Corrected from UTC to Aus EST  <<<<< STILL SOME CONFUSION ABOUT THIS
    return (Record_time)    # Do_Timestamp()
	
def twos_complement(value, bitWidth):
##########################################
    if value >= 2**bitWidth:
        # This catches value that is out of range
        raise ValueError("Value: {} out of range of {}-bit value.".format(value, bitWidth))
    else:
        return value - int((value << 1) & 2**bitWidth)    # twos_complement()
	
def Displacement(valu):
    """
    Decode real time data to Displacements_List in metres

    See DWTP 2.1.1 p20. Formula 16

    Args:
        Nil. 
        
    Returns:
        Calculated displacement.
   """    
    
    return (0.457*math.sinh(valu/457.))    # Displacement()
	
def Get_Displacements(Position, Sample_number, Displacements_List,Status,heave,north,west): 
##########################################   
    """
    Read displacement data and convert to actual values
    Refer to DWTP pp.19-21 AND Datawell Waverider RX-C4 Receiver Manual p.38
    """
    Step_back = int(Sample_number / 2)
    n = Position[0] - Step_back + 1
    for __ in range(Step_back):

##        line_number = Cycle_counter[m]
        displacement_record = Displacements_List[n+__]
        status_byte = displacement_record[0:1]
        Status.append(status_byte)
        record_0 = displacement_record[1:3]
        record_1 = displacement_record[3:5]
        record_2 = displacement_record[5:7]
        record_3 = displacement_record[7:9]
        record_4 = displacement_record[9:11]
        record_5 = displacement_record[11:13]
        record_6 = displacement_record[13:15]
        record_7 = displacement_record[15:17]
        record_8 = displacement_record[17:19]
        
# Build the 12-bit HEX value from the 8-bit records 0 to 8        
        h0 = record_0+record_1[0]; n0 = record_1[1]+record_2; w0 = record_3+record_4[0]
        h1 = record_4[1]+ record_5; n1 = record_6+record_7[0]; w1= record_7[1]+record_8
        
### Need to include check for NAN here !!!

# Convert the 12-bit HEX value into a signed integer, and then into actual displacement value
        h0_sign = Displacement(twos_complement(int(h0,16),12))
        n0_sign = Displacement(twos_complement(int(n0,16),12))
        w0_sign = Displacement(twos_complement(int(w0,16),12))
        h1_sign = Displacement(twos_complement(int(h1,16),12))
        n1_sign = Displacement(twos_complement(int(n1,16),12))
        w1_sign = Displacement(twos_complement(int(w1,16),12))
        
# Build lists holding each of the displacement values
        heave.append(h0_sign); heave.append(h1_sign) 
        north.append(n0_sign); north.append(n1_sign)
        west.append(w0_sign); west.append(w1_sign)

    return(Status,heave,north,west)    # Get_Displacements()
	
def Calc_MWL(heave):
##########################################
    """
    Calculate correction for mean water level (vide Goda p.320)
    """
    j = len(heave)
    N0 = 0; N1 = 0; N2 = 0; Y0 = 0; Y1 = 0; Y2 = 0
    for __ in range(0,j):
        N0 = N0 + __**0; N1 = N1 + __**1; N2 = N2 + __**2
        Y0 = Y0 + __**0*heave[__]; Y1 = Y1 + __**1*heave[__]; Y2 = Y2 + __**2*heave[__]    

    A0 = (N2*Y0 - N1*Y1)/(N0*N2 - N1**2); A1 = (N0*Y1 - N1*Y0)/(N0*N2 - N1**2)
    
    heave_mean = 0
    for __ in range(0,j):
        heave_mean = A0 + A1*__
            
    heave = [x+heave_mean for x in heave]   # apply the correction to the heave

    return(heave)    # Calc_MWL()
	
def calc_frequencies():
##########################################
    fk = []
    for __ in range(100):
        if __<46: fk.append(0.025 + 0.005*__)
        if __>=46 and __<79: fk.append(-0.20 + 0.010*__)
        if __>=79: fk.append(-0.98 + 0.020*__)
    
    return(fk)    # calc_frequencies()
	
def calc_spectral_parameters(heave,Sample_frequency):
##########################################
    f_Mk4 = calc_frequencies()    # calculate frequency values for the Mk4 wave buoy
                    
##    f1, Pden1 = sp.signal.periodogram(heave[0:4096],Sample_frequency,scaling='density',window=tukey(4096))
    f2, Pden2 = sp.signal.welch(heave,fs=Sample_frequency,window='hanning',nperseg=512,noverlap=256,\
            nfft=None,detrend='constant',return_onesided=True,scaling='density',axis=-1)
                       
# Peak frequency and period of banded spectra (Fp)                
    Fp2 = f2[Pden2.argmax()]; Tp2 = 1/Fp2

    return(f2,Pden2,Tp2)   # calc_spectral_parameters()	
	
def Mk4_moments(f_Mk4, Spectra):
##########################################
    """
    Routine to calculate the spectral moments: m0; m1; and m2 from an input Mk4 spectra
    """   
    Ax1 = (f_Mk4[45] - f_Mk4[0]) / 45
    Ax2 = (f_Mk4[78] - f_Mk4[45]) / 32
    Ax3 = (f_Mk4[99] - f_Mk4[78]) / 21 
    
# calc spectral moments m0, m1, and m2
    s00 = 0; s10 = 0; s20 = 0; m0 = 0
    s01 = 0; s11 = 0; s21 = 0; m1 = 0
    s02 = 0; s12 = 0; s22 = 0; m2 = 0
    for __ in range(1,44):
        s00 += Spectra[__]
        s01 += f_Mk4[__] * Spectra[__]
        s02 += f_Mk4[__]**2 * Spectra[__]
    m0 = 0.5*Ax1*(Spectra[0] + 2*s00 + Spectra[45])
    m1 = 0.5*Ax1*(f_Mk4[0]*Spectra[0]+2*s01+f_Mk4[45]*Spectra[45])
    m2 = 0.5*Ax1*(f_Mk4[0]**2*Spectra[0]+2*s02+f_Mk4[45]**2*Spectra[45])
    
    for __ in range(46,77):
        s10 += Spectra[__]
        s11 += f_Mk4[__] * Spectra[__]
        s12 += f_Mk4[__]**2 * Spectra[__]
    m0 += 0.5*Ax2*(Spectra[45] + 2*s10 + Spectra[78])
    m1 += 0.5*Ax2*(f_Mk4[45]*Spectra[45]+2*s11+f_Mk4[78]*Spectra[78])
    m2 += 0.5*Ax2*(f_Mk4[45]**2*Spectra[45]+2*s12+f_Mk4[78]**2*Spectra[78])
    
    for __ in range(79,98):
        s20 += Spectra[__]
        s21 += f_Mk4[__] * Spectra[__]
        s22 += f_Mk4[__]**2 * Spectra[__]
    m0 += 0.5*Ax3*(Spectra[78] + 2*s20 + Spectra[99])
    m1 += 0.5*Ax3*(f_Mk4[78]*Spectra[78]+2*s21+f_Mk4[99]*Spectra[99])
    m2 += 0.5*Ax3*(f_Mk4[78]**2*Spectra[78]+2*s22+f_Mk4[99]**2*Spectra[99])
    
    return (m0,m1,m2)   # Mk4_moments()	
	
def get_parameters_Mk4(f,Spectra,Record_time):
##########################################

    m0,m1,m2 = Mk4_moments(f,Spectra)    
    
# calc wave parameters Hm0, Hrms, T01, T02
    Hm0 = 4*np.sqrt(m0); Hrms = np.sqrt(2)/2*Hm0
    T01 = m0/m1; T02 = np.sqrt(m0/m2)
    
    # identify spectral peak and frequency as peak    
    max_frequency = [k for k, j in enumerate(Spectra) if j == np.max(Spectra)]
    Fp = f[max_frequency[0]]; Tp = 1/Fp 
    
    print('{:21s}{:<5.2f}{:6s}{:<5.2f}{:5s}{:<5.2f}{:5s}{:<5.2f}{:4s}{:<5.2f}'\
        .format('                 Hm0=',Hm0,' Hrms=',Hrms,' T01=',T01,' T02=',T02,' Tp=',Tp))

    return(Hm0,Hrms,T01,T02,Tp,Fp)   # get_parameters_Mk4()	
	
def find_median(sorted_list):
##########################################
    """
    Routine to calculate Median and first and third Quartiles in order to apply IQR Rule to heave.    
    """
    indices = []

    list_size = len(sorted_list)
    median = 0

    if list_size % 2 == 0:
        indices.append(int(list_size / 2) - 1)  # -1 because index starts from 0
        indices.append(int(list_size / 2))

        median = (sorted_list[indices[0]] + sorted_list[indices[1]]) / 2
        pass
    else:
        indices.append(int(list_size / 2))

        median = sorted_list[indices[0]]
        pass

    return(median,indices)    # find_median()
	
def Medcouple (heave,Q1,Q3,IQR):
##########################################
    """
    Following code based on IQR Rule and discussion at:
    https://stats.stackexchange.com/questions/13086/is-there-a-boxplot-variant-for-poisson-distributed-data#13429
    """
    import statsmodels
    from statsmodels.stats.stattools import medcouple

    M = medcouple(heave)
    if M >= 0:
        lower = Q1-math.exp(-4.0*M)*2*IQR
        upper = Q3+math.exp(3.0*M)*2*IQR
    else:
        lower = Q1-math.exp(-3.0*M)*2*IQR
        upper = Q3+math.exp(4.0*M)*2*IQR

    return(max(upper,abs(lower)))    # Medcouple()
	
def Do_error_checks(heave,Hsig,Hmax,Hrms,Hm0,THsig,T02,Tz,Tp,Tzmax):
##########################################
    from itertools import groupby

# look for consecutive WSEs the same - Warn if >= 4
    grouped_heave = [(k, sum(1 for i in g)) for k,g in groupby(heave)]
# Or (k, len(list(g))), but that creates an intermediate list
#    print('Warning - ',np.around(max(grouped_heave)[0],decimals=1),' occurred on ',max(grouped_heave)[1],' consecutive WSEs')
    if max(grouped_heave)[1] >= 4:
        print('Warning - ',np.around(max(grouped_heave)[0],decimals=1),' occurred on ',max(grouped_heave)[1],' consecutive WSEs')

# Check Hmax/Hsig ratio - limits are very broad
    if Hmax < 1.39*Hsig: print('Warning - Hmax < 1.39*Hsig')
    if Hmax > 2.20*Hsig: print('Warning - Hmax > 2.20*Hsig')
        
# Check Hrms/Hsig ratio
    if Hrms < 0.67*Hsig: print('Warning - Hrms < 0.67*Hsig')
    if Hrms > 0.75*Hsig: print('Warning - Hrms > 0.75*Hsig')
        
# Check Hm0/Hsig ratio
    if Hm0 < Hsig: print('Warning - Hm0 < Hsig')
    if Hm0 > Hsig*(1.05+(0.25/(Hsig+1))): print('Warning - Hm0 > Hsig*(1.05+(0.25/(Hsig+1)))')
        
# Check T02/THsig ratio
    if T02 < (THsig + 2.75)/(THsig * 2.5): print('Warning - T02 < (THsig + 2.75)/(THsig * 2.5)')
    if T02 > 0.95*THsig: print('Warning - T02 > 0.95*THsig')
                   
# Check period limits
    if Tz < 2 or Tz > 14: print('Warning - Tz < 2s or Tz > 14s')
    if THsig < 2 or THsig > 14: print('Warning - THsig < 2s or THsig > 14s')
    if T02 < 2 or T02 > 14: print('Warning - T02 < 2s or T02 > 14s')
    if Tp < 2 or Tp > 16: print('Warning - Tp < 2s or Tp > 16s')
    if Tzmax < 2 or Tzmax > 20: print('Warning - Tzmax < 2s or Tzmax > 20s')
    if Hsig > 5: print('Warning - Hsig > 5m')
        
    return()    # Do_error_checks()	
	
def Fill_Gaps(Displacements_List,Position,Position_Bad,Cycle_DF):
##########################################
    Bad_Displacements_List = ['-FFFFFFFFFFFFFFFFFF' for i in range(2304)]
    j = 2303
    for i in range(Position[0],Position_Bad,-1):
        Diff = Cycle_DF[i]-Cycle_DF[i-1]
        if Diff == 1 or Diff == -255: 
            Bad_Displacements_List[j] = Displacements_List[i]
            j-=1
        else:
            Bad_Displacements_List[j] = Displacements_List[i]
            j-=Diff
    return(Bad_Displacements_List)    # Fill_Gaps()	
		
def Do_F20(packet):
##########################################
    """
    Decode Heave spectrum message vide DWTP p. 38-40
    """
    Do_CRC(packet)
    Record_time = Do_Timestamp(packet)
    Data_stamp = packet[6]+packet[7]
    No_of_segments = int(packet[8],16)
    if No_of_segments == 255:                   # Check for NaN
        print("Number of segments recorded on buoy = NaN")
    Smax = int(packet[9]+packet[10][0],16)
    if Smax == 4095:                   # Check for NaN
        print("Smax = NaN"); Smax = -99.99
    else:
        Smax = 5000*(math.exp(Smax/200.) - 1) / (math.exp(4094/200.) - 1)
    s_tilde = []
    for __ in range(0,148,3):
        u = int(packet[11+__]+packet[12+__][0],16)
        s_tilde.append(Smax*(math.exp(u/200.) - 1) / (math.exp(4094/200.) - 1))
        u = int(packet[12+__][1]+packet[13+__],16)
        s_tilde.append(Smax*(math.exp(u/200.) - 1) / (math.exp(4094/200.) - 1))
##        print('{:18s}{:%Y-%m-%d %H:%M}{:6s}'.format('Heave spectrum at ',Record_time,' AEST:'))
##        print('{:27s}{:10.4f}{:2s}{:1s}{:3s}'.format('Peak value of PSD (Smax) = ',Smax,' m',chr(178),'/Hz'))

    return (Record_time,s_tilde,Smax,No_of_segments)    # Do_F20()
	
def Do_F21(packet):
##########################################
    """
    Decode Heave spectrum message vide DWTP p. 40-41
    """
    Do_CRC(packet)
    Record_time = Do_Timestamp(packet)
    Data_stamp = packet[6]+packet[7]
    No_of_segments = int(packet[8],16)
    if No_of_segments == 255:                   # Check for NaN
        print("Number of segments recorded on buoy = NaN")
    
    Dirn = []; Spread = []
    
    for __ in range(0,298,3):
        u = int(packet[9+__]+packet[10+__][0],16)
        Dirn.append(math.degrees(u/4095.*2.*math.pi))
        u = int(packet[10+__][1]+packet[11+__],16)
        Spread.append(math.degrees(u/4095.*math.pi/2.))

    return (Record_time,Dirn,Spread)    # Do_F21()	
	
def Do_F23(packet):
##########################################
    """
    Decode Spectrum synchronisation message vide DWTP p. 45-47
    """    
##    print('In Spectrum synchronisation routine')
    Do_CRC(packet)
    Record_time = Do_Timestamp(packet)
    Data_stamp = packet[6]+packet[7]
    Segments_used = '0000000'+packet[8][:-1]+packet[9]+packet[10]
    Sample_number = int(packet[11]+packet[12],16)        
    if Sample_number == 65536:                   # Check for NaN
        print("Timestamp = NaN")
    else:
        Hn_1 = packet[13]+packet[14][0]; Nn_1 = packet[14][1]+packet[15]; Wn_1 = packet[16]+packet[17][0]
        Hn = packet[17][1]+packet[18]; Nn = packet[19]+packet[20][0]; Wn = packet[20][1]+packet[21]
    
        Match_vector = Hn_1+Nn_1+Wn_1+Hn+Nn+Wn  # build a HEX string to be matched with heave list
        Hn_1_sign = Displacement(twos_complement(int(Hn_1,16),12))        # Uses formula 16 from DWTP p. 20
        Nn_1_sign = Displacement(twos_complement(int(Nn_1,16),12))
        Wn_1_sign = Displacement(twos_complement(int(Wn_1,16),12))

        Hn_sign = Displacement(twos_complement(int(Hn,16),12))
        Nn_sign = Displacement(twos_complement(int(Nn,16),12))
        Wn_sign = Displacement(twos_complement(int(Wn,16),12))

    if Sample_number > 4608: Sample_number = 4608    # NEED TO RESOLVE THIS ISSUE
    return (Record_time,Match_vector,Sample_number,Hn_1_sign,Nn_1_sign,Wn_1_sign,Hn_sign,Nn_sign,Wn_sign)    # Do_F23()
	
def Do_F25(packet):
##########################################
    """
    Decode Directional Spectral Parameters message vide DWTP p. 51-53
    """    
    Do_CRC(packet)
    Record_time = Do_Timestamp(packet)
    Data_stamp = packet[6]+packet[7]
    No_of_segments = int(packet[8],16)
    if No_of_segments == 255:                   # Check for NaN
        print("Number of segments = NaN")
    Hs = int(packet[9]+packet[10][0],16)
    Ti = int(packet[10][1]+packet[11],16)
    Te = int(packet[12]+packet[13][0],16)
    T1 = int(packet[13][1]+packet[14],16)
    Tz = int(packet[15]+packet[16][0],16)
    T3 = int(packet[16][1]+packet[17],16)
    Tc = int(packet[18]+packet[19][0],16)
    Rp = int(packet[19][1]+packet[20],16)
    Tp = int(packet[21]+packet[22][0],16)
    Smax = int(packet[22][1]+packet[23],16)
    Theta_p = int(packet[24]+packet[25][0],16)
    Sigma_p = int(packet[25][1]+packet[26],16)
    
# Check for NaN    
    if Hs == 4095: print("Hs = NaN") 
    else: Hs = Hs/100.
    if Ti == 4095: print("Ti = NaN") 
    else: Ti = Ti/100.
    if Te == 4095: print("Te = NaN") 
    else: Te = Te/100.        
    if T1 == 4095: print("T1 = NaN") 
    else: T1 = T1/100.
    if Tz == 4095: print("Tz = NaN") 
    else: Tz = Tz/100.
    if T3 == 4095: print("T3 = NaN") 
    else: T3 = T3/100.
    if Tc == 4095: print("Tc = NaN") 
    else: Tc = Tc/100.
    if Rp == 4095: print("Rp = NaN")                 # See note with p.50 Formula 65 re Goda's peakedness
    else: Rp = Rp/4094.
    if Tp == 4095: print("Tp = NaN") 
    else: Tp = Tp/100.
    if Smax == 4095: print("Smax = NaN") 
    else: Smax = 5000*(math.exp(Smax/200.) - 1) / (math.exp(4094/200.) - 1)
    if Theta_p == 4095: print("Theta_p = NaN") 
    else: Theta_p = math.degrees(Theta_p/4095.*2.*math.pi)    # Note - Theta_p converted to Degrees!!!
    if Sigma_p == 4095: print("Sigma_p = NaN") 
    else: Sigma_p = math.degrees(Sigma_p/4095.*math.pi/2.)    # Note - Sigma_p converted to Degrees!!!
    
#    print('{:%Y-%m-%d %H:%M}{:5s}{:5.2f}{:5s}{:5.2f}{:5s}{:5.2f}{:5s}{:5.2f}{:5s}{:5.2f}{:5s}{:5.2f}{:5s}{:5.2f}' \
#        .format(Record_time,' Hs= ',Hs,' Ti= ',Ti,' Te= ',Te,' T1= ',T1,' Tz= ',Tz,' T3= ',T3,' Tc= ',Tc))
#    print('{:>27s}{:5.2f}{:5s}{:5.2f}{:7s}{:5.4f}{:10s}{:5.4f}{:1s}{:10s}{:5.4f}{:1s}' \
#        .format(' Rp= ',Rp,' Tp= ',Tp,' Smax= ',Smax,' Theta_p= ',Theta_p,chr(176),' Sigma_p= ',Sigma_p,chr(176)))
    
    return (Record_time,Hs,Ti,Te,T1,Tz,T3,Tc,Tp)    # Do_F25()
	
def Do_F26(packet):
##########################################
    """
    Decode online upcrossing wave statistics message vide DWTP p. 52-57
    """    
##    print('In online upcrossing wave statistics routine')
    Do_CRC(packet)
    Record_time = Do_Timestamp(packet)
    Data_stamp = packet[6]+packet[7]
    Hmax = int(packet[8]+packet[9][0],16); THmax = int(packet[9][1]+packet[10],16); Tmax = int(packet[11]+packet[12][0],16)
    HTmax = int(packet[12][1]+packet[13],16); Havg = int(packet[14]+packet[15][0],16); Tavg = int(packet[15][1]+packet[16],16)
    Hsrms = int(packet[17]+packet[18][0],16); Nw = int(packet[18][1]+packet[19],16); Nc = int(packet[20]+packet[21][0],16)
    Epsilon = int(packet[21][1]+packet[22],16); Coverage = int(packet[23]+packet[24][0],16)
    
# Check for NaN    
    if (Hmax == 4095): print('Hmax = NaN') 
    if THmax == 4095: print('THmax = NaN')
    if Tmax == 4095: print('Tmax = NaN')
    if (HTmax == 4095): print("HTmax = NaN")
    if (Havg == 4095): print("Havg = NaN")
    if (Tavg == 4095): print("Tavg = NaN")
    if (Hsrms == 4095): print("Hsrms = NaN")
    if (Nw == 4095): print("Nw = NaN")
    if (Nc == 4095): print("Nc = NaN")
    if (Epsilon == 4095): print("Epsilon = NaN") 
    if (Coverage == 4095): print("Coverage = NaN")
    
    Hmax = Hmax / 100.; THmax = THmax / 100.; Tmax = Tmax / 100. 
    HTmax = HTmax / 100.; Havg = Havg / 100.; Tavg = Tavg / 100.
    Hsrms = Hsrms / 100.; Epsilon = Epsilon / 4094.; Coverage = Coverage / 4094. * 100. * 100.
    
#    print('{:%Y-%m-%d %H:%M}{:7s}{:5.2f}{:8s}{:5.2f}{:7s}{:5.2f}{:8s}{:5.2f}{:9s}{:5.2f}' \
#        .format(Record_time,' Hmax= ',Hmax,' THmax= ',THmax,' Tmax= ',Tmax,' HTmax= ',HTmax,' Hsrms= ',Hsrms))
#    print('{:>28s}{:4d}{:>9s}{:4d}{:>14s}{:5.4f}{:>15s}{:5.2f}{:1s}'.format(' Nw= ',Nw,' Nc= ',Nc,'Epsilon= ',Epsilon,' Coverage = ',Coverage,'%'))
    
    return (Record_time,Hmax,THmax,Tmax,HTmax,Hsrms,Nw,Nc)   # Do_F26()
	
def Do_F29(packet):
##########################################
    """
    Decode Upcrossing wave height quantiles message vide DWTP p. 58-62
    """    
##    print('In Upcrossing wave height quantiles routine')
    import math
    Do_CRC(packet)
    Record_time = Do_Timestamp(packet)
    Data_stamp = packet[6]+packet[7]
    Coverage = int(packet[8]+packet[9][0],16)
    if Coverage == 4095:                   # Check for NaN
        print("Coverage = NaN"); Coverage = -99.99
    Nw = int(packet[9][1]+packet[10],16)
    Epsilon = int(packet[11]+packet[12][0],16)
    Hmax = int(packet[12][1]+packet[13],16)
    THmax = int(packet[14]+packet[15][0],16)
    H10 = int(packet[15][1]+packet[16],16)
    TH10 = int(packet[17]+packet[18][0],16)
    Hthird = int(packet[18][1]+packet[19],16)
    THthird = int(packet[20]+packet[21][0],16)
    Havg = int(packet[21][1]+packet[22],16)
    Tavg = int(packet[23]+packet[24][0],16)
    Hq0 = int(packet[24][1]+packet[25],16)
    Hq1 = int(packet[26]+packet[27][0],16)
    Hq2 = int(packet[27][1]+packet[28],16)
    
    Hq = []
##    print(packet)
    for __ in range(0,30,3):
        u = int(packet[24+__][1]+packet[25+__],16)
        Hq.append(u/100.)
        u = int(packet[26+__]+packet[27+__][0],16)
        Hq.append(u/100.)
        
# Check for NaN    
    if (Hmax == 4095): print('Hmax = NaN') 
    if THmax == 4095: print('THmax = NaN')
    if H10 == 4095: print('H10 = NaN')
    if (TH10 == 4095): print("TH10 = NaN")
    if (Hthird == 4095): print("Hthird = NaN")
    if (THthird == 4095): print("THthird = NaN")
    if (Havg == 4095): print("Havg = NaN")
    if (Tavg == 4095): print("Tavg = NaN")
        
    Hmax = Hmax / 100.; THmax = THmax / 100.
    H10 = H10 / 100.; TH10 = TH10 / 100. 
    Hthird = Hthird / 100.; THthird = THthird / 100. 
    Havg = Havg / 100.; Tavg = Tavg / 100.        
##    print('{:%Y-%m-%d %H:%M}{:7s}{:5.2f}{:7s}{:5.2f}{:6s}{:5.2f}{:7s}{:5.2f}{:8s}{:5.2f}{:8s}{:5.2f}{:7s}{:5.2f}{:8s}{:5.2f}'\
##          .format(Record_time,' Havg= ',Havg,' H1/3= ',Hthird,' H10= ',H10,' Hmax= ',Hmax,' THavg= ',Havg,' TH1/3= ',THthird,' TH10= ',H10,' THmax= ',THmax))
    
    return ()    # Do_F29()
    
def Do_time_domain(heave,Record_time,Tp2,Displacements_List,Position):
##########################################
    heave = np.array(heave)

# Identify and report where Repaired values; Bad values; and Spikes are in record
    Times = np.linspace(0,1800,num=4608)
    Repaired_vals = [i for i,x in enumerate([x[0:1]for x in Displacements_List[Position[0]:Position[0]+2304]]) if x == '=']
    Repaired_vals = np.array(Repaired_vals)
    np.set_printoptions(precision=2)
    if len(Repaired_vals)>0: print('Repaired values at ',Times[Repaired_vals[:]])
                    
    Bad_vals = [i for i,x in enumerate([x[0:1]for x in Displacements_List[Position[0]:Position[0]+2304]]) if x == '!']
    Bad_vals = np.array(Bad_vals)
    np.set_printoptions(precision=2)
    if len(Bad_vals)>0: print('WARNING: Bad values at ',Times[Bad_vals[:]])

# NOTE - used Median for spike detection instead of Mean (as Median is more resistant to influence of outliers)        
##    spikes = np.where(np.abs(heave) > np.median((heave)+3.5*np.std(heave)))    
#######################################
    sorted_heave = np.sort(heave)
    median, median_indices = find_median(sorted_heave)
    Q1, Q1_indices = find_median(sorted_heave[:median_indices[0]])
    Q3, Q3_indices = find_median(sorted_heave[median_indices[-1] + 1:])
    IQR = Q3-Q1
    spikes = np.where(np.abs(heave) > Medcouple(heave,Q1,Q3,IQR)) 
#######################################                      
    spike_locations = np.array(Times[spikes[:][0]])
    np.set_printoptions(precision=2)
    if (len(spike_locations) > 0): 
        print('ALERT: Possible spikes in the data at times:',spike_locations)
        heave = np.delete(heave,spikes[:][0])
    
# Identify zero-crossing points to be used in identifying individual waves
# From Goda 2nd. edition p.321 10.10, and also includes test for zero-crossing where wl = 0
    zero_crossing = []; valid_zero_crossing = []
    for __ in range(len(heave)-2):
        if (heave[__]*heave[__+1] < 0 and heave[__+1] > 0) or (heave[__] == 0 and heave[__-1] < 0 and heave[__+1] > 0):
            zero_crossing.append(__)    
    
    zero_crossing = np.array(zero_crossing)
    waves = []; Periods = []; Wave_num = 0; Crest_num = 0
    i = 0; j = i+1
    try:
        while i in range(len(zero_crossing)-2):
            wave = heave[zero_crossing[i]+1:zero_crossing[j]+1]
            crest = max(wave); trough = min(wave)

            if i > 0 and ((crest <= 0.05 and trough >= -0.05) or crest <= 0.05):  
# No wave, or no crest above threshold
# So, we need to step back to the start of the previous wave for the crest and trough details. 
# Wave period needs to be expanded to include period of last wave AND period of this event too.
    ##            print('{:5d}{:31s}{:10.4f}{:10.4f}'.format(i,'   No wave or no crest detected',crest,trough))
    ##            print('--------------------------------------------------')
                waves = waves[:-1]                 # remove last recorded wave from list
                Periods = Periods[:-1]             # remove last recorded Period from list
                Wave_num -=1                       # reduce count of number of waves by 1
                if len(valid_zero_crossing) > 0: del valid_zero_crossing[-1]        # remove this zero crossing from list as wave declared invalid
                if crest <= 0.05: Crest_num -=1    # reduce count of number of Crests by 1
                i -= 1
            elif (trough >= -0.05):                # No trough below threshold
    ##            print('{:5d}{:22s}{:10.4f}{:10.4f}'.format(i,'   No trough detected ',crest,trough))
    ##            print('--------------------------------------------------')
                j += 1
            else:                                  # wave exceeds thresholds, so process it
                Height = crest+abs(trough)
                waves.append(Height)
                valid_zero_crossing.append(zero_crossing[i])  # keep record of zero-crossings for valid waves      
    # get the individual wave periods.
    # the sums of increments of wse's plus linear interpolation of the zero-crossing end bits            
                if heave[zero_crossing[i]]!=0:
                    H1 = heave[zero_crossing[i]+1]+abs(heave[zero_crossing[i]])
                    h1 = abs(heave[zero_crossing[i]])
                    delta_h1= float(h1*0.39)/float(H1)
                else:
                    delta_h1=0      

                if heave[zero_crossing[j]]!=0:
                    H2 = heave[zero_crossing[j]+1]+abs(heave[zero_crossing[j]+1])
                    h2 = abs(heave[zero_crossing[j]+1])
                    delta_h2= float(h2*0.39)/float(H2)
                else:
                    delta_h2=0

                period = Times[zero_crossing[j]+1]-Times[zero_crossing[i]+1]-delta_h1+delta_h2
                Periods.append(period)
    #            print('{:10d}{:10.4f}{:10.4f}{:10.4f}{:10.4f}'.format(i+1,heave[zero_crossing[i]],heave[zero_crossing[j]],Height,period))
    #            print('{:5d}{:3s}{:10.4f}{:3s}{:10.4f}{:3s}{:10.4f}{:3s}{:10.4f}'.format(i+1,'H1=',H1,'h1=',h1,'H2=',H2,'h2=',h2))
                Wave_num +=1; Crest_num += 1
    ##            print('{:5d}{:5d}{:10.4f}{:10.4f}'.format(i,Wave_num,Height,period))
                i=j; j+=1    # can now move to next zero-crossing wave
    except IndexError:
        pass       
# Sort the waves and determine Time-Domain heights
    waves = np.asarray(waves)
    sorted_waves = np.sort(waves)[::-1]
    Hmax = max(sorted_waves)
    Hmean = mean(sorted_waves)
    Hsig = mean(sorted_waves[0:math.trunc(len(sorted_waves)/3.0)])
    H10 = mean(sorted_waves[0:math.trunc(len(sorted_waves)/10.0)])
    Hrms = np.sqrt(np.mean(sorted_waves**2))

# Get Time-Domain periods
    Periods = np.asarray(Periods)
    Tmax = max(Periods)
    Tz = mean(Periods)
    THmax = Periods[np.abs(waves - Hmax).argmin()]
    THsig = Periods[np.abs(waves - Hsig).argmin()]
    TH10 = Periods[np.abs(waves - H10).argmin()]

    print('{:22s}{:<5.2f}{:6s}{:<5.2f}{:6s}{:<5.2f}{:4s}{:<5.2f}{:6s}{:<5.2f}{:4s}{:<5.2f}{:6s}{:<5.2f}{:11s}{:<4d}\
    '.format('                 Hsig=',Hsig,' Hrms=',Hrms,' Hmax=',Hmax,' Tz=',Tz,' THsig=',THsig,' Tp=',Tp2,' Tmax=',Tmax,' Wave Num.=',Wave_num))
    valid_zero_crossing = np.array(valid_zero_crossing)
    
    return(valid_zero_crossing,Hmean,Hsig,Hmax,Tz,THsig,Tp2,Tmax,waves,spikes)   # Do_time_domain()