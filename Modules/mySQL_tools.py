# -*- coding: utf-8 -*-
"""
    mySQL Tools

    A series of tools designed to perform various tasks with the mySQL databases

    Author: Jim Waldron
    Date: December 2019
    
    Includes:
    get_sites()
    get_site_number()
    get_data()
    get_start_end_dates()
    
"""
import mysql.connector
from mysql.connector import errorcode

import pandas as pd
from datetime import datetime, time, timedelta

from tkinter import filedialog
from tkinter import *



def get_sites(database, host, urN, Pwd):
#################################################################
    """
    Retrieves a list of sites and there associate site numbers from OndasNueva.SiteName database table
    
    Calls: nothing
    
    Returns: 
    """

    try:
        cnx = mysql.connector.connect(user=urN, password =Pwd,
                                          host=host, database=database,
                                          auth_plugin='mysql_native_password') 
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
            
    cursor = cnx.cursor()
    query = ("SELECT siteTitle, siteNumber FROM SiteName")
    cursor.execute(query)
    
    sites = pd.DataFrame(cursor.fetchall())
    sites.columns = cursor.column_names
    sites_db = sites

    cursor.close
    cnx.close
    
    return(sites_db)    # getsites()
    
def close():
    '''
    Clean up TKinter loop
    '''

    global lb, root_1, item1
    
    try:
        item1 = lb.get(lb.curselection())
    except:
        item1 = 'Nothing selected'
    root_1.destroy()
    
    return(item1)    # close()

def get_site_number(sites_db):
#######################################################
    '''
    Select site details from db using a listbox.
    Still very much under development!!!
    
    Calls: nothing
    
    Returns: Site name and Site number
    '''

    global lb, root_1, item1
    
    item1 = ''
    while item1 == '':    
    # Choose a site
        root_1 = Tk()
        root_1.title("Site selection")

        scrollbar = Scrollbar(root_1, orient="vertical")
        lb = Listbox(root_1, width=30, height=30, font=('Aerial',10),justify='left',yscrollcommand=scrollbar.set)
        scrollbar.config(command=lb.yview)

        scrollbar.pack(side="right", fill="y")
        lb.pack(side="left",fill="both", expand=True)

        site_list = sites_db.siteTitle.values
        for item in site_list:
            lb.insert('end', item)
        lb.focus()

        b = Button(root_1, text = "GO", command = close).pack()

        try:
            root_1.mainloop()

        except:
            pass
        
        close
        
    return(item1,sites_db.loc[sites_db.siteTitle == item1].siteNumber.values.astype('str')[0])    # get_site_number()    
    
def get_data(siteName, siteno, startdate, enddate, database, table, host, urN, Pwd):  
#################################################################
    """
    Retrieves data from input database for the required dates
    
    Example call: sql.get_data("Brisbane","4183","2019-09-13 00:00","2019-09-14 00:00","OndasCuatro","SiteSummaryData","localhost","dbuser","password")
     
    Calls: nothing
   
    Returns: dataframe of selected data
    
    startdate, enddate are both strings
    
    JW - December 2019
    """
    print("In get_data: Reading data from ",database,".",table)
    
    try:
        cnx = mysql.connector.connect(user=urN, password =Pwd,
                                          host=host, database=database,
                                          auth_plugin='mysql_native_password') 
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    
    cursor = cnx.cursor()
    
    Start_Date = datetime.strptime(startdate, "%Y-%m-%d %H:%M")
    End_Date = datetime.strptime(enddate, "%Y-%m-%d %H:%M")
    
    print(Start_Date,End_Date)
    
    query1 = ("SELECT * FROM " + table + " where siteNumber = %s AND from_unixtime(recordSeconds) BETWEEN %s AND %s")    
    
    cursor.execute(query1, (int(siteno), Start_Date, End_Date))
    
    rows_count = cursor.fetchall()
    if not rows_count:
        noData = True
        info = False
        return noData
    else:
        noData = False
        info = True
        cursor.execute(query1, (int(siteno), Start_Date, End_Date))

        df = pd.DataFrame(cursor.fetchall())
        df.columns = cursor.column_names
    cursor.close()
    cnx.close()
    
    return(df)    # get_data()
    
def get_start_end_dates(siteName, siteno,  database, table, host, urN, Pwd):  
#################################################################
    """
    Retrieves start and end dates from input database table
    
    Example call: sql.get_start_end_dates("Brisbane","4183","OndasNueva","RawData","localhost","dbuser","password")  
    
    Calls: nothing
       
    Returns: start and end dates of data in selected table
    
    JW - December 2019
    """
    
    print("In get_start_end_dates: Reading data from ",database,".",table)
    
    try:
        cnx = mysql.connector.connect(user=urN, password =Pwd,
                                          host=host, database=database,
                                          auth_plugin='mysql_native_password') 
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    
    cursor = cnx.cursor()
          
    query1 = ("SELECT FROM_UNIXTIME(MIN(recordSeconds)), FROM_UNIXTIME(MAX(recordSeconds)) FROM  " + database+"."+table+" where siteNumber = "+siteno)

    cursor.execute(query1)

    rows = cursor.fetchall()

    if len(rows) ==0:
        noData = True
        info = False
        print(noData)
    else:
        noData = False
        info = True
        Start_Date, End_Date = rows[0][0],rows[0][1]

    print(Start_Date,End_Date)

    cursor.close()
    cnx.close()
    
    return(Start_Date, End_Date)    # get_start_end_dates()
