# -*- coding: utf-8 -*-
"""
Wave Analysis Tools

A series of tools designed to perform wave analysis on .HXV files from datawell Mk3 and GPS Waverider buoys

Author: Jim Waldron

Latest update: December 2019
"""

import binascii
import math
import numpy as np
import scipy as sp
import sys

from datetime import datetime, time, timedelta
from math import exp
from scipy import fftpack, signal
from scipy.signal import tukey
from scipy.stats import norm,rayleigh
from tkinter import filedialog
from tkinter import *

Sample_frequency = "global"
Sample_rate = "global"
Sample_frequency = 1.28   # Use 1.28Hz for Mk3 and DWG buoys
Sample_rate = 1. / Sample_frequency

def Get_real_time_data(SSNN,HHHN,NNWW,WPPP):
##########################################
    Heave = []; North = []; West = []
    rec_length = len(SSNN)
   
    for i in range(rec_length):
        vertical = int(HHHN[i][0:3],16)
        if vertical >= 2048: vertical = 2048 - vertical
        north = int(HHHN[i][3]+NNWW[i][0:2],16)
        if north >= 2048: north = 2048 - north
        west = int(NNWW[i][2:4]+WPPP[i][0],16)
        if west >= 2048: west = 2048 - west
        Heave.append(vertical/100.); North.append(north/100.); West.append(west/100.)
        
    return(Heave,North,West)

def Hex2Bin(valu):
##########################################
    return bin(int('1'+valu,16))[3:]

def Process_Cyclic_Data(Start_time,YYYY):
##########################################
    Sync_word = [i for i, j in enumerate(YYYY) if j == '7FFF'] # Locate all records whose line number is 00
    Record_time = Start_time + timedelta(seconds=(Sync_word[0]/1.28)) # Record time starts from first 512-sample (200s record)

    System_word_number = []; System_word = [] 
    Frequency = []; Direction = []; RPSD = []; Spread = []; M2 = []; N2 = []; Check_factor = []

    for i in range(len(Sync_word)-1):
        System_word_number.append(YYYY[Sync_word[i]+1][0]); System_word.append(YYYY[Sync_word[i]+1][1:4])
    ###    print(System_word_number[i],System_word[i])
        for j in range(0,16,4): # repeat this loop 4 times - see Table 5.7.2
            Vector = Hex2Bin(YYYY[Sync_word[i]+2+j])
            freq_index = int(Vector[2:8],2) 

    ###        if freq_index == 0:

            if freq_index <= 15:
                freq = 0.025 + freq_index*0.005
            else:
                freq = 0.11+ (freq_index-16)*0.01

            dir = int(Vector[8:16],2)*360./256.
            Slsb = int(Vector[0:2],2); Frequency.append(freq); Direction.append(dir)
            Vector = Hex2Bin(YYYY[Sync_word[i]+3+j]); pdens = exp((0-int(Vector[4:16],2)/200.))
            M2lsb = int(Vector[0:2],2); N2lsb = int(Vector[2:4],2);  RPSD.append(pdens)
            Vector = Hex2Bin(YYYY[Sync_word[i]+4+j]); spread = int(Vector[0:8],2)+Slsb/4.
            Spread.append(spread); M2.append((int(Vector[8:16],2)+M2lsb/4. - 128)/128.)
            Vector = Hex2Bin(YYYY[Sync_word[i]+5+j])
            N2.append((int(Vector[0:8],2)+N2lsb/4. - 128)/128.); Check_factor.append(int(Vector[8:16],2)/100.)

##            print('{:5d}{:1s}{:5.3f}{:1s}{:6.5f}{:1s}{:6.2f}{:1s}{:6.2f}'\
##                  .format(freq_index,' ',freq,' ',pdens,' ',dir,' ',spread))

    ### IMPORTANT NOTE: See Datawell Waverider Reference Manual DWR-MkIII Section 5.7.1.4 on p.48
    ### There will be 8 spectra collected every 30 mins.

    start = np.where(np.around(Frequency,decimals=3) == 0.025)
    finish =np.where(np.around(Frequency,decimals=3) == 0.58)
    
    Latest_Frequency = Frequency[start[0][-2]:start[0][-1]]
    Latest_RPSD = RPSD[start[0][-2]:start[0][-1]]
    Latest_Direction = Direction[start[0][-2]:start[0][-1]]
    Latest_Spread = Spread[start[0][-2]:start[0][-1]]
    
    ###################################################################
    # Process System data words - refer to Section 5.7.1.3 System File
    ###################################################################
    System_word_number = [int(i,16) for i in System_word_number]
    Hm0 = []; fz = []; Tz = []; PSDmax = []; Tr = []; Tw = []; B = []; tol = []; Lat = []; Long = []
    got_LatMSB = 'False'    # eed to set flag in case Latitude LSB is read before Latitude MSB (see below)

    for i in System_word_number:    
    ###    print(int(System_word_number[i],16),' ',System_word[i])
        if System_word_number[i] == 1:
            hrms = int(System_word[i],16)/400; Hm0.append(hrms * 4.)
        if System_word_number[i] == 2:
            freq_zero = int(Hex2Bin(System_word[i])[4:12],2)/400
            tz = 1/freq_zero
            fz.append(freq_zero); Tz.append(tz)
        if System_word_number[i] == 3:
            psd_max = 5000.*exp(-int(System_word[i],16)/200.)
            PSDmax.append(psd_max)
        if System_word_number[i] == 4:
            Tr.append(int(Hex2Bin(System_word[i])[2:12],2)/20-5)
        if System_word_number[i] == 5:
            Tw.append(int(Hex2Bin(System_word[i])[2:12],2)/20-5)
        if System_word_number[i] == 6:    
            B.append(int(Hex2Bin(System_word[i])[9:12],2))     # Need to see Section 5.10.2 
            tol.append(int(Hex2Bin(System_word[i])[0:8],2))    # Need to see Section 5.10.2 
        if System_word_number[i] == 10:
            got_LatMSB = 'True'    # Raise flag to indicate that LatMSB will be available in calculations below
            sign = Hex2Bin(System_word[i])[0]
            LatMSB = Hex2Bin(System_word[i])[1:12]
        if System_word_number[i] == 11:
            LatLSB = Hex2Bin(System_word[i])
            if got_LatMSB:    # Only process the latitude if we have both LatLSB AND LatMSB
                latitude = (int(LatMSB+LatLSB,2)/2**23)*90
                if sign == '1': # code for Southern Hemisphere
                    latitude = -latitude 
                Lat.append(latitude)
                got_LatMSB = 'False'    # Reset flag
        if System_word_number[i] == 12:
            sign = Hex2Bin(System_word[i])[0]
            LongMSB = Hex2Bin(System_word[i])[1:12]
        if System_word_number[i] == 13:
            LongLSB = Hex2Bin(System_word[i])
            longitude = (int(LongMSB+LongLSB,2)/2**23)*180
            Long.append(longitude)

# Need to sort out issue with values getting out of order            
    Spectra = list(zip(Latest_Frequency,Latest_RPSD))    # Convert two lists into a tuple
    Spectra.sort(key=lambda tup: tup[0])                 # Sort the tuple on frequency
    for i in range(64):                                  # Return the sorted values to their respective lists
        Latest_Frequency[i] = Spectra[i][0]
        Latest_RPSD[i] = Spectra[i][1]

    Tp = 1./Latest_Frequency[Latest_RPSD.index(max(Latest_RPSD))]   # Calculate Tp from Datawell spectra        
    
    return(Record_time,Hm0,Tz,PSDmax,Tr,Tw,B,Latest_Direction,Latest_Spread,Lat,Long,Latest_Frequency,Latest_RPSD,Tp)    # Process_Cyclic_Data()

def Do_time_domain(wls,Record_time,Tp):
##########################################
    dt = Sample_rate                   # Sample rate ~ 0.78s = 1.28Hz
    period_sum = 0
    wls = np.array(wls)

    Times = np.linspace(0,len(wls)*dt,num=len(wls))

# Identify Spikes in record
    spikes = np.where(np.abs(wls) > np.median((wls)+3.5*np.std(wls)))    # NOTE - used Median instead of Mean (as median is more resistant to outliers)
    spike_locations = np.array(Times[spikes[:][0]])
    np.set_printoptions(precision=2)
    if (len(spike_locations) > 0): 
        print('ALERT: Possible spikes in the data at times:',spike_locations)
        wls = np.delete(wls,spikes[:][0])
        Times = np.delete(Times,spikes[:][0])
   
# Locate zero-crossing points to be used in identifying individual waves
# From Goda 2nd. edition p.321 10.10, and also includes test for zero-crossing where wl = 0
    zero_crossing = []; valid_zero_crossing = []
    for i in range(len(wls)-2):
        if (wls[i]*wls[i+1] < 0 and wls[i+1] > 0) or (wls[i] == 0 and wls[i-1] < 0 and wls[i+1] > 0):
            zero_crossing.append(i)    
    
    zero_crossing = np.array(zero_crossing)
    waves = []; Periods = []; Wave_num = 0; Crest_num = 0; Rejected_waves = 0
    i = 0; j = i+1; End_val = 2
    while i in range(len(zero_crossing)-End_val):
        wave = wls[zero_crossing[i]+1:zero_crossing[j]+1]
        crest = max(wave); trough = min(wave)
        
        if i > 0 and ((crest <= 0.01 and trough >= -0.01) or crest <= 0.01):  # No wave, or no crest above threshold
# So, we need to step back to the start of the previous wave for the crest and trough details. 
# Wave period needs to be expanded to include period of last wave AND period of this event too.
            if (crest <= 0.01 and trough >= -0.01):
                pass    # Comment this out if use next linepass
#                print('{:5d}{:31s}{:10.4f}{:10.4f}'.format(i,'   No wave detected ',crest,trough))
#                print('--------------------------------------------------')
            else:
                pass    # Comment this out if use next line
#                print('{:5d}{:31s}{:10.4f}{:10.4f}'.format(i,'   No crest detected',crest,trough))
            waves = waves[:-1]                 # remove last recorded wave from list
            Periods = Periods[:-1]             # remove last recorded Period from list
            Wave_num -=1                       # reduce count of number of waves by 1
            if len(valid_zero_crossing) > 0: del valid_zero_crossing[-1]        # remove this zero crossing from list as wave declared invalid
            if crest <= 0.01: Crest_num -=1    # reduce count of number of Crests by 1
            i -= 1
            Rejected_waves+=1
        elif (trough >= -0.01):                # No trough below threshold
#            print('{:5d}{:31s}{:10.4f}{:10.4f}'.format(i,'   No trough detected ',crest,trough))
#            print('--------------------------------------------------')
            waves = waves[:-1]                 # remove last recorded wave from list
            Periods = Periods[:-1]             # remove last recorded Period from list
            Wave_num -=1                       # reduce count of number of waves by 1
            if len(valid_zero_crossing) > 0: del valid_zero_crossing[-1]        # remove this zero crossing from list as wave declared invalid
            j += 1
            End_val += 1     # Need this so we don't move past end of loop counter
            Rejected_waves+=1
        else:                                  # wave exceeds thresholds, so process it
            Height = crest+abs(trough)
            waves.append(Height)
            valid_zero_crossing.append(zero_crossing[i])  # keep record of zero-crossings for valid waves      
# get the individual wave periods.
# the sums of increments of wse's plus linear interpolation of the zero-crossing end bits            
            if wls[zero_crossing[i]] != 0:
                H1 = wls[zero_crossing[i]+1] + abs(wls[zero_crossing[i]])
                h1 = wls[zero_crossing[i]]
                delta_h1 = abs(float(h1*dt)/float(H1))
            else:
                delta_h1 = 0      

            if wls[zero_crossing[j]+1] != 0:
                H2 = wls[zero_crossing[j]+1] + abs(wls[zero_crossing[j]])
                h2 = wls[zero_crossing[j]]
                delta_h2 = abs(float(h2*dt)/float(H2))
            else:
                delta_h2 = 0

#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<        
            period = (Times[zero_crossing[j]] - Times[zero_crossing[i]]) - delta_h1 + delta_h2
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

            Periods.append(period)
            period_sum+=period
#            print('{:10d}{:10.4f}{:10.4f}{:10.4f}{:10.4f}'.format(i+1,wls[zero_crossing[i]],wls[zero_crossing[j]],Height,period))
#            print('{:5d}{:3s}{:10.4f}{:3s}{:10.4f}{:3s}{:10.4f}{:3s}{:10.4f}'.format(i+1,'H1=',H1,'h1=',h1,'H2=',H2,'h2=',h2))
            Wave_num += 1; Crest_num += 1
##            print('{:5d}{:5d}{:10.4f}{:10.4f}'.format(i,Wave_num,Height,period))
            i = j; j += 1    # can now move to next zero-crossing wave
        
# Sort the waves and determine Time-Domain heights
    waves = np.asarray(waves)
    sorted_waves = np.sort(waves)[::-1]
    Hmax = max(sorted_waves)
    Hsig = np.mean(sorted_waves[0:math.trunc(len(sorted_waves)/3)])
    H10 = np.mean(sorted_waves[0:math.trunc(len(sorted_waves)/10.0)])
    Hrms = np.sqrt(np.mean(sorted_waves**2))

# Get Time-Domain periods
    Periods = np.asarray(Periods)
    Tmax = max(Periods)
    Tz = np.mean(Periods)
    THmax = Periods[np.abs(waves - Hmax).argmin()]
    THsig = Periods[np.abs(waves - Hsig).argmin()]
    TH10 = Periods[np.abs(waves - H10).argmin()]

    print('{:%Y-%m-%d %H:%M}{:6s}{:<5.2f}{:6s}{:<5.2f}{:6s}{:<5.2f}{:4s}{:<5.2f}{:7s}{:<5.2f}{:4s}{:<5.2f}{:6s}{:<5.2f}{:7s}{:<4d}{:10s}{:<4d}\
        '.format(Record_time,' Hsig=',Hsig,' Hrms=',Hrms,' Hmax=',Hmax,' Tz=',Tz,' THsig=',THsig,' Tp=',Tp,' Tmax=',Tmax,' Waves=',Wave_num,' Rejected=',Rejected_waves))
    
    return(Hsig,Hrms,Hmax,waves,spikes)   # Do_time_domain()
    
def calc_spectral_parameters(wls,Sample_frequency):
##########################################
    f_avg = []; Pden_avg = []
                    
    f1, Pden1 = sp.signal.periodogram(wls[0:2048],Sample_frequency,scaling='density',window=tukey(2048))
    f2, Pden2 = sp.signal.welch(wls[0:2048],fs=Sample_frequency,window='hanning',nperseg=256,noverlap=0,\
            nfft=None,detrend='constant',return_onesided=True,scaling='density',axis=-1)
                    
    f_avg.append(0); Pden_avg.append(np.mean(Pden1[0:9]))
    for i in range(1,128,1): 
        f_avg.append(f1[i*8]); Pden_avg.append(np.mean(Pden1[i*8-4:i*8+4]))

# Peak frequency and period of banded spectra (Fp)                
    Fp1 = f_avg[Pden_avg.index(max(Pden_avg))]; Tp1 = 1/Fp1
    Fp2 = f2[Pden2.argmax()]; Tp2 = 1/Fp2

    return(f_avg,Pden_avg,f1,Pden1,f2,Pden2)   # calc_spectral_parameters()

def Banded_moments(f_Banded, Spectra):
##########################################
    """
    routine to calculate the spectral moments: m0; m1; and m2 from an input Mk3 spectra
    """
    
    Ax = (f_Banded[127] - f_Banded[0]) / 126
    
# calc spectral moments m0, m1, and m2
    s0 = 0; s1 = 0; s2 = 0;
    for j in range(1,126):
        s0 += Spectra[j]
        s1 += f_Banded[j] * Spectra[j]
        s2 += f_Banded[j]**2 * Spectra[j]
    m0 = 0.5*Ax*(Spectra[0] + 2*s0 + Spectra[127])
    m1 = 0.5*Ax*(f_Banded[0]*Spectra[0]+2*s1+f_Banded[127]*Spectra[127])
    m2 = 0.5*Ax*(f_Banded[0]**2*Spectra[0]+2*s2+f_Banded[127]**2*Spectra[127])
    
    return (m0,m1,m2)   # Banded_moments()

def get_parameters_Banded(f,Spectra):
##########################################
    m0,m1,m2 = Banded_moments(f,Spectra)    
    
# calc wave parameters Hm0, Hrms, T01, T02
    Hm0 = 4*np.sqrt(m0); Hrms = np.sqrt(2)/2*Hm0
    T01 = m0/m1; T02 = np.sqrt(m0/m2)
    
    # identify spectral peak and frequency as peak    
    max_frequency = [k for k, j in enumerate(Spectra) if j == np.max(Spectra)]
    Fp = f[max_frequency[0]]; Tp = 1/Fp 

    return(Hm0,Hrms,T01,T02,Tp,Fp)   # get_parameters_Banded()    