# ### Outlier detection modules
# #### JW 01/19

#get_ipython().run_line_magic('matplotlib', 'inline')

# # Investigate outlier detection using Mahalanobis Distance
################################################################
# JW note: See https://www.machinelearningplus.com/statistics/mahalanobis-distance/
import pandas as pd
import scipy as sp
import numpy as np

def mahalanobis(x=None, data=None, cov=None):
##########################################
    """Compute the Mahalanobis Distance between each row of x and the data  
    x    : vector or matrix of data with p columns.
    data : ndarray of the distribution from which Mahalanobis distance of each observation of x is to be computed.
    cov  : covariance matrix (p x p) of the distribution. If None, will be computed from data.
    """
    x_minus_mu = x - np.mean(data)
    if not cov:
        cov = np.cov(data.values.T)
    inv_covmat = sp.linalg.inv(cov)
    left_term = np.dot(x_minus_mu, inv_covmat)
    mahal = np.dot(left_term, x_minus_mu.T)
    
    return mahal.diagonal()
    
def test_mahalanobis(Heave,North,West,Sample_frequency):  
##########################################
    dt = 1/Sample_frequency
    j = len(Heave)
    Times = np.linspace(0,j*dt,num=j) 
    Time = np.linspace(0,j*dt,num=j)

    df = pd.DataFrame({'Time': Time,'Heave': Heave, 'North': North, 'West': West}) 
    df_x = df[['Heave', 'North', 'West']]
    df_x['mahala'] = mahalanobis(x=df_x, data=df[['Heave', 'North', 'West']])
    df_x['Time'] = Time

    # Critical values for two degrees of freedom
    from scipy.stats import chi2
    chi2.ppf((1-0.001), df=2)

    # Compute the P-Values
    df_x['p_value'] = 1 - chi2.cdf(df_x['mahala'], 2)

    # Extreme values with a significance level of 0.001
    Suspect = df_x.loc[df_x.p_value < 0.001]

    found = len(Suspect)
    print('Suspect records from multivariate anomaly detection using Mahalanobis distance.\n')

    if found > 0:
        print('     Time  Heave  North   West Mahala')
        print('     ===============================')
        for i in range(found):
            print('{:>9.2f}{:>7.2f}{:>7.2f}{:>7.2f}{:>7.2f}'.format(Suspect.Time.iloc[i],Suspect.Heave.iloc[i],Suspect.North.iloc[i],Suspect.West.iloc[i],Suspect.mahala.iloc[i]))
    else:
        print('No suspects found!')
        
    return()
     

from statsmodels.formula.api import ols

def do_Regression(Displacement,Sample_frequency):
##########################################
# # Investigate outlier detection using Ordinary Least Squares and Outlier regression
################################################################
    """
    Identifies outliers in Heave, North, and West displacements using Ordinary Least Squares and Outlier regression

    Parameters:
    -----------
        Displacement - an array of Heaves
        
    Returns:
    --------
    

    References:
    ----------
        https://stackoverflow.com/questions/10231206/can-scipy-stats-identify-and-mask-obvious-outliers
    
    """
    dt = 1/Sample_frequency
    j = len(Displacement)
    x = np.linspace(0,j*dt,num=j) 

    # Make Heave fit #
    regression = ols("data ~ x", data=dict(data=Displacement, x=x)).fit()

    # Find outliers #
    test = regression.outlier_test()
    return(test)


def OLS(Heave,North,West):
##########################################
    
    Heave_outliers = do_Regression(Heave)
    print ('Heave Outliers: ', list(Heave_outliers))
    plt.scatter(*zip(*Heave_outliers),marker='*')

    North_outliers = do_Regression(North)
    print ('North Outliers: ', list(North_outliers))
    plt.scatter(*zip(*North_outliers),marker='*')

    West_outliers = do_Regression(West)
    print ('West Outliers: ', list(West_outliers))
    if len(West_outliers) > 0: plt.scatter(*zip(*West_outliers),marker='*')
    
    return()


import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from sklearn.decomposition import FastICA, PCA

def test_ica_pca(Heave,North,West):
##########################################
    """
    Investigate outlier detection using ICA and PCA
    JW - Work in progress
    see https://scikit-learn.org/stable/auto_examples/decomposition/plot_ica_blind_source_separation.html#sphx-glr-auto-examples-decomposition-plot-ica-blind-source-separation-py
    """

    n_samples = len(Heave)
    dt = 1/1.28
    j = len(Heave)
    times = np.linspace(0.0,j*dt,num=j)
        
    S = np.c_[Heave, North, West]
    #S += 0.2 * np.random.normal(size=S.shape)  # Add noise

    S /= S.std(axis=0)  # Standardize data

    # Mix data
    A = np.array([[1, 1, 1], [0.5, 2, 1.0], [1.5, 1.0, 2.0]])  # Mixing matrix
    X = np.dot(S, A.T)  # Generate observations

    # Compute ICA
    ica = FastICA(n_components=3)
    S_ = ica.fit_transform(X)  # Reconstruct signals
    A_ = ica.mixing_  # Get estimated mixing matrix

    # We can `prove` that the ICA model applies by reverting the unmixing.
    assert np.allclose(X, np.dot(S_, A_.T) + ica.mean_)

    # For comparison, compute PCA
    pca = PCA(n_components=3)
    H = pca.fit_transform(X)  # Reconstruct signals based on orthogonal components

    # #############################################################################
    # Plot results

    plt.figure(figsize=(20, 20))

    #plt.figure()

    models = [X, S, S_, H]
    names = ['Observations (mixed signal)',
             'True Sources',
             'ICA recovered signals',
             'PCA recovered signals']
    colors = ['red', 'steelblue', 'orange']

    for ii, (model, name) in enumerate(zip(models, names), 1):
        plt.subplot(4, 1, ii)
        plt.title(name)
        for sig, color in zip(model.T, colors):
            plt.plot(sig, color=color)

    plt.subplots_adjust(0.09, 0.04, 0.94, 0.94, 0.26, 0.46)
    plt.show()

    return()

def test_covariance(Heave,West):
##########################################
    """
    Investigate outlier detection using Empirical Covariance, Robust Covariance, and One Class SVM
    JW - Work in progress
    """

    print(__doc__)

    # Author: Virgile Fritsch <virgile.fritsch@inria.fr>
    # License: BSD 3 clause

    import numpy as np
    from sklearn.covariance import EllipticEnvelope
    from sklearn.svm import OneClassSVM
    import matplotlib.pyplot as plt
    import matplotlib.font_manager
    from sklearn.datasets import load_boston

    # Get data
    xx = np.array([Heave,West]) 
    X1 = np.reshape(xx,(len(Heave), 2))
    Heave_limit = round(max(max(Heave),abs(min(Heave)))+0.5)
    West_limit = round(max(max(West),abs(min(West)))+0.5)

    # Define "classifiers" to be used
    classifiers = {
        "Empirical Covariance": EllipticEnvelope(support_fraction=None, contamination=0.01),
        "Robust Covariance (Minimum Covariance Determinant)": EllipticEnvelope(contamination=0.01),
        "One Class SVM": OneClassSVM(nu=0.01, gamma='auto')}
    colors = ['m', 'g', 'b']
    legend1 = {}

    # Learn a frontier for outlier detection with several classifiers
    xx1, yy1 = np.meshgrid(np.linspace(-West_limit, West_limit, 500), np.linspace(-Heave_limit, Heave_limit, 500))

    # Plot the results (= shape of the data points cloud)
    plt.figure(figsize=(10,10),dpi=100)  # two clusters
    plt.title("Outlier detection on Heave v's East-West")
    plt.scatter(Heave, West, edgecolors='k', cmap='jet')  #, color='lightblue',s=3,alpha=0.75)

    for i, (clf_name, clf) in enumerate(classifiers.items()):
    #    plt.figure(1)
        clf.fit(X1)
        Z1 = clf.decision_function(np.c_[xx1.ravel(), yy1.ravel()])
        Z1 = Z1.reshape(xx1.shape)
        legend1[clf_name] = plt.contour(
            xx1, yy1, Z1, levels=[0], linewidths=1, colors=colors[i],alpha=0.5)

    legend1_values_list = list(legend1.values())
    legend1_keys_list = list(legend1.keys())

    bbox_args = dict(boxstyle="round", fc="0.8")
    arrow_args = dict(arrowstyle="->")

    plt.xlim((xx1.min(), xx1.max()))
    plt.ylim((yy1.min(), yy1.max()))

    plt.grid(True,c='grey',alpha=0.25,zorder=0)

    plt.legend((legend1_values_list[0].collections[0],
                legend1_values_list[1].collections[0],
                legend1_values_list[2].collections[0]),
               (legend1_keys_list[0], legend1_keys_list[1], legend1_keys_list[2]),
               loc="upper center",
               prop=matplotlib.font_manager.FontProperties(size=12), frameon=False)
    plt.ylabel("Heave")
    plt.xlabel("West")

    plt.show()
    return()    # test_covariance()
    
## JW - Work in progress
import numpy as np

from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

def test_pca(Heave,West):
##########################################

    # Define the PCA object
    pca = PCA()

    # Get data
    xx = np.array([Heave, West]) 
    X1 = np.reshape(xx,(len(Heave), 2))

    # Run PCA on scaled data and obtain the scores array
    T = pca.fit_transform(StandardScaler().fit_transform(X1))

    # Score plot of the first 2 PC
    fig = plt.figure(figsize=(15,10))

    with plt.style.context(('ggplot')):
        plt.scatter(T[:, 0], T[:, 1], edgecolors='k', cmap='jet')
        plt.xlabel('PC1')
        plt.ylabel('PC2')
        plt.title('Score Plot')
        
    fig = plt.figure(figsize=(15,10))
    with plt.style.context(('ggplot')):
        plt.scatter(T[:, 0], T[:, 1], edgecolors='k', cmap='jet')
        plt.xlim((-10, 10))
        plt.ylim((-10, 10))
        plt.xlabel('PC1')
        plt.ylabel('PC2')
        plt.title('Score Plot')
        
    # Compute the euclidean distance using the first 2 PC
    euclidean = np.zeros(X1.shape[0])
    for i in range(2):
        euclidean += (T[:,i] - np.mean(T[:,:2]))**2/np.var(T[:,:2])
        
    colors = [plt.cm.jet(float(i)/max(euclidean)) for i in euclidean]
    fig = plt.figure(figsize=(15,10))
    with plt.style.context(('ggplot')):
        plt.scatter(T[:, 0], T[:, 1], c=colors, edgecolors='k', s=60)
        plt.xlabel('PC1')
        plt.ylabel('PC2')
        plt.xlim((-10, 10))
        plt.ylim((-10, 10))
        plt.title('Score Plot')    
        
    from sklearn.covariance import EmpiricalCovariance, MinCovDet

    # fit a Minimum Covariance Determinant (MCD) robust estimator to data 
    robust_cov = MinCovDet().fit(T[:,:1])

    # Get the Mahalanobis distance
    m = robust_cov.mahalanobis(T[:,:1])    

    #plt.update_layout(plot_bgcolor='white')
    plt.show()
    
    return()    # test_pca()


def covariance_SVM(Heave,West):
##########################################
    """
    Investigate outlier detection using Robust covariance and One-Class SVM 
    JW - Work in progress
    Author: Alexandre Gramfort <alexandre.gramfort@inria.fr>
            Albert Thomas <albert.thomas@telecom-paristech.fr>
    License: BSD 3 clause
    """

    import time

    import numpy as np
    import matplotlib
    import matplotlib.pyplot as plt

    from sklearn import svm
    from sklearn.datasets import make_moons, make_blobs
    from sklearn.covariance import EllipticEnvelope
    from sklearn.ensemble import IsolationForest
    from sklearn.neighbors import LocalOutlierFactor

    print(__doc__)

    matplotlib.rcParams['contour.negative_linestyle'] = 'solid'

    # Get data
    xx = np.array([Heave, West]) 
    X1 = np.reshape(xx,(len(Heave), 2))

    n_samples = len(Heave)
    outliers_fraction = 0.01
    n_outliers = int(outliers_fraction * n_samples)
    n_inliers = n_samples - n_outliers

    # define outlier/anomaly detection methods to be compared
    anomaly_algorithms = [
        ("Robust covariance", EllipticEnvelope(contamination=outliers_fraction)),
        ("One-Class SVM", svm.OneClassSVM(nu=outliers_fraction, kernel="rbf", gamma=0.1)),
    #    ("Isolation Forest", IsolationForest(behaviour='new', contamination=outliers_fraction, random_state=42)),
    #    ("Local Outlier Factor", LocalOutlierFactor(n_neighbors=35, p=2, metric='euclidean', contamination=outliers_fraction))
        ]

    # Define datasets
    blobs_params = dict(random_state=0, n_samples=n_inliers, n_features=2)
    datasets = [
        make_blobs(centers=[[0, 0], [0, 0]], cluster_std=0.5,
                   **blobs_params)[0],
        make_blobs(centers=[[2, 2], [-2, -2]], cluster_std=[0.5, 0.5],
                   **blobs_params)[0],
        make_blobs(centers=[[2, 2], [-2, -2]], cluster_std=[1.5, .3],
                   **blobs_params)[0]]
        
    ##    4. * (make_moons(n_samples=n_samples, noise=.05, random_state=0)[0] -
    ##          np.array([0.5, 0.25])),
    ##    14. * (np.random.RandomState(42).rand(n_samples, 2) - 0.5)]

    # Compare given classifiers under given settings
    xx, yy = np.meshgrid(np.linspace(-5, 5, 500), np.linspace(-5, 5, 500))

    plt.figure(figsize=(len(anomaly_algorithms) * 8 + 3, 25))
    plt.subplots_adjust(left=.02, right=.98, bottom=.001, top=.96, wspace=.05,
                        hspace=.01)

    plot_num = 1

    for i_dataset, X in enumerate(datasets):
        # Add outliers
        X = X1

        for name, algorithm in anomaly_algorithms:
            t0 = time.time()
            algorithm.fit(X)
            t1 = time.time()
            plt.subplot(len(datasets), len(anomaly_algorithms), plot_num)
            if i_dataset == 0:
                plt.title(name, size=14)

            # fit the data and tag outliers
            if name == "Local Outlier Factor":
                y_pred = algorithm.fit_predict(X)
            else:
                y_pred = algorithm.fit(X).predict(X)

            # plot the levels lines and the points
            if name != "Local Outlier Factor":  # LOF does not implement predict
                Z = algorithm.predict(np.c_[xx.ravel(), yy.ravel()])
                Z = Z.reshape(xx.shape)
                plt.contour(xx, yy, Z, levels=[0], linewidths=1, colors='purple')

            colors = np.array(['red', 'green'])
            plt.scatter(X[:, 0], X[:, 1], s=15, color=colors[(y_pred + 1) // 2],alpha=0.5)

            plt.xlim(-7, 7)
            plt.ylim(-7, 7)
            plt.xticks(())
            plt.yticks(())
            plt.text(.99, .01, ('%.2fs' % (t1 - t0)).lstrip('0'),
                     transform=plt.gca().transAxes, size=15,
                     horizontalalignment='right')
            plot_num += 1

    plt.show()
    
    return()    # covariance_SVM

################################################################
# Median Absolute Deviation!
#import numpy as np
#from statsmodels import robust

#robust.mad(Heave)
################################################################


def ransac(Heave,North):
##########################################
    """
    Testing RANSAC algorithm
    JW - Work in progress
    """
    import numpy as np
    from matplotlib import pyplot as plt

    from sklearn import linear_model, datasets

    n_samples = 1000
    n_outliers = 50


    X, y, coef = datasets.make_regression(n_samples=n_samples, n_features=1,
                                          n_informative=1, noise=10,
                                          coef=True, random_state=0)
    X = Heave; y = North
    # Add outlier data
    np.random.seed(0)
    #X[:n_outliers] = 3 + 0.5 * np.random.normal(size=(n_outliers, 1))
    #y[:n_outliers] = -3 + 10 * np.random.normal(size=n_outliers)

    # Fit line using all data
    lr = linear_model.LinearRegression()
    lr.fit(X, y)

    # Robustly fit linear model with RANSAC algorithm
    ransac = linear_model.RANSACRegressor()
    ransac.fit(X, y)
    inlier_mask = ransac.inlier_mask_
    outlier_mask = np.logical_not(inlier_mask)

    # Predict data of estimated models
    line_X = np.arange(X.min(), X.max())[:, np.newaxis]
    line_y = lr.predict(line_X)
    line_y_ransac = ransac.predict(line_X)

    # Compare estimated coefficients
    print("Estimated coefficients (true, linear regression, RANSAC):")
    print(coef, lr.coef_, ransac.estimator_.coef_)

    lw = 2
    plt.figure(figsize=(15, 10))
    plt.scatter(X[inlier_mask], y[inlier_mask], color='yellowgreen', marker='.',
                label='Inliers')
    plt.scatter(X[outlier_mask], y[outlier_mask], color='gold', marker='.',
                label='Outliers')
    plt.plot(line_X, line_y, color='navy', linewidth=lw, label='Linear regressor')
    plt.plot(line_X, line_y_ransac, color='cornflowerblue', linewidth=lw,
             label='RANSAC regressor')
    plt.legend(loc='lower right')
    plt.xlabel("Input")
    plt.ylabel("Response")
    from statsmodels.formula.api import ols
    plt.show()
    
    return()


