# -*- coding: utf-8 -*-
"""Wave Plotting Tools

A series of tools designed to perform various plots of wave data

Author: Jim Waldron
Date: December 2019
"""

#%matplotlib inline

import array as arr
import binascii
import colorama
import itertools
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import scipy as sp
import scipy.stats
#import statistics as s
import sys
import tkinter as tk

import HVA_tools as ht

from colorama import Fore, Back, Style
from datetime import datetime, time, timedelta
from itertools import count # izip for maximum efficiency
from mpl_toolkits.mplot3d import Axes3D
from pathlib import Path, PureWindowsPath
from scipy import fftpack, signal
from scipy.signal import tukey
from scipy.stats import norm,rayleigh,skew,kurtosis
from statistics import mean
from tkinter import filedialog
from tkinter import *

def Do_time_series_plot(Start_time,Heave,North,West,spikes,Sample_frequency):
##########################################
    """
    Do time series plot of HXV data
    """
    dt = 1/Sample_frequency
    j = len(Heave)
    spikes = spikes[0][:]
    
# Populate an array with 4608 points at 1/2.56 spacing (i.e. 1800s)    
    Times = np.linspace(0,j*dt,num=j) 
    
#    x = [Start_time + timedelta(milliseconds=781.25*i) for i in range(j)]
    x = np.linspace(0,j*dt,num=j)
    plt.figure(figsize=(20, 10))

    bottom = min(min(Heave),min(North),min(West))*1.05
    top = max(max(Heave),max(North),max(West))*1.05
    
    plt.subplot(3,1,1)
    plt.title(Start_time,fontsize=12)
    plt.xlim(left=0,right=1800)
    plt.ylim(bottom=bottom,top=top)
    plt.grid(True)
    plt.plot(x,Heave,c='#99ebff',label='Heave',linewidth=0.75)
    if spikes.size:
        for __ in spikes:
                plt.plot(Times[__],Heave[__],linewidth=0,marker='o',markeredgecolor='r',markerfacecolor='none',markersize=5)
    plt.legend(loc='upper right',fontsize=12,frameon=False)
    
    plt.subplot(3,1,2)
    plt.xlim(left=0,right=1800)
    plt.ylim(bottom=bottom,top=top)
    plt.grid(True)
    plt.plot(x,North,c='xkcd:salmon',label='North-South',linewidth=0.75)
    plt.legend(loc='upper right',fontsize=12,frameon=False)
    
    plt.subplot(3,1,3)
    plt.xlim(left=0,right=1800)
    plt.ylim(bottom=bottom,top=top)
    plt.grid(True)
    plt.plot(x,West,c='xkcd:wheat',label='East-West',linewidth=0.75)
    plt.legend(loc='upper right',fontsize=12,frameon=False)
    plt.tight_layout()
    
    plt.show()
    
    return

def Do_spectral_plot(Record_time,freq,power,f_avg,Pden_avg,f2,Pden2):
##########################################
    """
    Do spectral plot of HXV data
    """
    plt.figure(figsize=(20, 8))
    plt.title(Record_time,fontsize=14)
    plt.xlabel('Frequency (Hz)',fontsize=14) # Display Frequency on X-axis
    plt.ylabel('Spectral density ($m^2$/Hz)',fontsize=16)
    plt.tick_params(axis='both', which='major', labelsize=15)
    plt.xticks(np.arange(0, max(freq)+1, 0.1))
    plt.grid(True)
    plt.xlim(left=0.0,right=0.6)
    plt.plot(freq,power,marker="o",linewidth=2,color='b',markeredgecolor='b',markerfacecolor='none',markersize=5,label='Datawell')
    plt.fill(freq,power,c='xkcd:light sky blue')
    plt.plot(f2,Pden2,linewidth=2,label="Welch's method")
    plt.plot(f_avg,Pden_avg,marker="o",linewidth=2,color='g',markeredgecolor='g',markerfacecolor='none',markersize=5,label='Band averaged method')
    plt.legend(loc='upper right',fontsize=16,frameon=False)
    plt.tight_layout()
    plt.show()

    return()    # Do_spectral_plot()

def Plot_spectra(f,Spectra,f2,Pden2,f3,Pxx_den,Date,Hm0,Hrms,T01,T02,Tp,Fp):
##########################################
    """
    Do spectral plot of HVA data
    """
    max_Spectra = max(max(Spectra),max(Pden2),max(Pxx_den))

    plt.figure(figsize=(20,10)) ###, dpi= 80, facecolor='w', edgecolor='k')
    plt.tick_params(axis='both', which='major', labelsize=15)
    plt.grid(True)
    plt.title(Date,fontsize=16)  
    plt.xlabel('Frequency (Hz)',fontsize=16); plt.xlim(0, 1) # Display Frequency on X-axis
    plt.ylim(bottom=0, top=max_Spectra)
    plt.ylabel('Spectral density ($m^2$/Hz)',fontsize=16)
    
    plt.text(0.45, max_Spectra*0.95, "Welch's method",horizontalalignment='left',verticalalignment='center',fontsize=15)
    plt.text(0.45, max_Spectra*0.90, 'Hm0  = '+"%.2f" % Hm0+'m',horizontalalignment='left',verticalalignment='center',fontsize=15)
    plt.text(0.45, max_Spectra*0.85, 'Hrms = '+"%.2f" % Hrms+'m',horizontalalignment='left',verticalalignment='center',fontsize=15)
    plt.text(0.45, max_Spectra*0.80, 'T01  = '+"%.2f" % T01+'s',horizontalalignment='left',verticalalignment='center',fontsize=15)
    plt.text(0.45, max_Spectra*0.75, 'T02  = '+"%.2f" % T02+'s',horizontalalignment='left',verticalalignment='center',fontsize=15)
    plt.text(0.45, max_Spectra*0.70, 'Tp   = '+"%.2f" % Tp+'s',horizontalalignment='left',verticalalignment='center',fontsize=15)
    plt.text(0.45, max_Spectra*0.65, 'Fp   = '+"%.2f" % Fp+'Hz',horizontalalignment='left',verticalalignment='center',fontsize=15)

    plt.plot(f,Spectra,'-b',linewidth=4,label = 'Datawell')
    plt.fill(f,Spectra,c='xkcd:light sky blue')
    plt.plot(f2,Pden2,'-r',linewidth=4,label = "Welch's method")
    plt.plot(f3,Pxx_den,'-g',lw=4,label="wavestatsbox",alpha=0.5)
    
    plt.legend(loc='upper right',fontsize=16,frameon=False)
    plt.tight_layout()
    plt.show()
    plt.close()
    
    return()   # Plot_spectra()
	
def Do_XYZ_Hist(Heave,North,West,date_str):
##########################################
    import plotly.figure_factory as ff
    import numpy as np

    x1 = Heave
    x2 = North
    x3 = West

    group_labels = ['Heave', 'North-South', 'East-West']

    colors = ['#99ebff', 'salmon', 'wheat']

    # Create distplot with curve_type set to 'normal'
    fig = ff.create_distplot([x1, x2, x3], group_labels, bin_size=.2,
                             curve_type='normal', colors=colors)

    # Add title
    fig.update_layout(title_text=date_str,height=800,width=1000,)
    fig.update_traces(opacity=0.5)
    fig.layout.template = 'plotly_white'
    fig.show()
    
    return()    # Do_XYZ_Hist()	
	
def Plot_heave(heave,Below,Hsig,Hmax,Tz,Tp,Record_time,spikes):
##########################################
    """
    plot time-series of current half-hour on four plots (each 450s wide).
    This plot shows WSEs and those points used to determine zero-upcrossing waves.
    """
    spikes = spikes[0][:]    # convert tuple to array
    heave = np.array(heave)
    
# Populate an array with 4608 points at 1/2.56 spacing (i.e. 1800s)    
    Times = np.linspace(0,1800,num=4608)
    if(len(heave)<len(Times)): 
        diff = len(Times) - len(heave)
# Identified an error in heave, so chop end off Times        
        Times = Times[0:len(heave)]
        print('ALERT: ',diff,' water-level values missing from file!')
    plt.figure(figsize=(20,15),dpi=100)
    plt.subplot(4, 1, 1)
    plt.title(Record_time,fontsize=10)
    plt.xlabel('Time from start of record (s)')
    x = 0; y = 1152
    top_val = round(Hmax+0.5)/2
        
    plt.xlim(left = 0,right = 450); bottom_val = -top_val
    plt.ylim(bottom = bottom_val,top = top_val)
    bb = (np.abs(Below-y)).argmin()
##  plt.plot(Times[Below[0:bb+1]],heave[Below[0:bb+1]],linewidth=0,c='xkcd:orangered',marker='o',markersize=4)
    if spikes.size:
        bb1 = spikes[np.nonzero((spikes>= x)&(spikes< y))]
        if bb1.size:
            for __ in bb1:
                plt.plot(Times[__],heave[__],linewidth=0,marker='o',markeredgecolor='r',markerfacecolor='none',markersize=10)
    plt.tick_params(axis='both', which='major', labelsize=10)
    plt.plot(Times[x:y+5],heave[x:y+5],linewidth=0.75,marker='x',markersize=1,c='xkcd:azure')
    plt.fill_between(Times[x:y+5], 0, heave[x:y+5], facecolor='lightblue')
    plt.plot([Times[x],Times[y+5]],[0.05,0.05],linewidth=0.25,c='xkcd:aquamarine')
    plt.plot([Times[x],Times[y+5]],[-0.05,-0.05],linewidth=0.25,c='xkcd:aquamarine')
    
    plt.text(1, top_val*0.8, '  Hsig = '+"%.2f" % Hsig+'m'+'    Hmax = '+"%.2f" % Hmax+'m'+\
             '    Tz = '+"%.2f" % Tz+'s'+'    Tp = '+"%.2f" % Tp+'s')
    plt.grid(True,linewidth=0.5)

    plt.subplot(4, 1, 2)
#    plt.title(Record_time,fontsize=15)
    plt.xlabel('Time from start of record (s)')
    x = 1152; y = 2304
    plt.xlim(left = 450,right = 900)
    plt.ylim(bottom = 0-round(Hmax+0.5)/2,top = round(Hmax+0.5)/2)
    aa = (np.abs(Below-x)).argmin()
    bb = (np.abs(Below-y)).argmin()
##  plt.plot(Times[Below[aa-1:bb+1]],heave[Below[aa-1:bb+1]],linewidth=0,c='xkcd:orangered',marker='o',markersize=4)
    if spikes.size:
        bb1 = spikes[np.nonzero((spikes>= x)&(spikes< y))]
        if bb1.size:
            for __ in bb1:
                plt.plot(Times[__],heave[__],linewidth=0,marker='o',markeredgecolor='r',markerfacecolor='none',markersize=10)
    plt.plot(Times[x-5:y+5],heave[x-5:y+5],linewidth=0.75,marker='x',markersize=1,c='xkcd:azure')
    plt.fill_between(Times[x-5:y+5], 0, heave[x-5:y+5], facecolor='lightblue')
    plt.plot([Times[x-5],Times[y+5]],[0.05,0.05],linewidth=0.25,c='xkcd:aquamarine')
    plt.plot([Times[x-5],Times[y+5]],[-0.05,-0.05],linewidth=0.25,c='xkcd:aquamarine')
    plt.tick_params(axis='both',which='major',labelsize=10)
    plt.grid(True,linewidth=0.5)

    plt.subplot(4, 1, 3)
#    plt.title(Record_time,fontsize=15)1
    plt.xlabel('Time from start of record (s)')
    x = 2304; y = 3456
    plt.xlim(left = 900,right = 1350)
    plt.ylim(bottom = 0-round(Hmax+0.5)/2,top = round(Hmax+0.5)/2)
    aa = (np.abs(Below-x)).argmin()
    bb = (np.abs(Below-y)).argmin()
##  plt.plot(Times[Below[aa-1:bb+1]],heave[Below[aa-1:bb+1]],linewidth=0,c='xkcd:orangered',marker='o',markersize=4)
    if spikes.size:
        bb1 = spikes[np.nonzero((spikes>= x)&(spikes< y))]
        if bb1.size:
            for __ in bb1:
                plt.plot(Times[__],heave[__],linewidth=0,marker='o',markeredgecolor='r',markerfacecolor='none',markersize=10)
    plt.plot(Times[x-5:y+5],heave[x-5:y+5],linewidth=0.75,marker='x',markersize=1,c='xkcd:azure')
    plt.fill_between(Times[x-5:y+5], 0, heave[x-5:y+5], facecolor='lightblue')    
    plt.plot([Times[x-5],Times[y+5]],[0.05,0.05],linewidth=0.25,c='xkcd:aquamarine')
    plt.plot([Times[x-5],Times[y+5]],[-0.05,-0.05],linewidth=0.25,c='xkcd:aquamarine')
    plt.tick_params(axis='both',which='major',labelsize=10)
    plt.grid(True,linewidth=0.5)

    plt.subplot(4, 1, 4)
#    plt.title(Record_time,fontsize=15)
    plt.xlabel('Time from start of record (s)')
    x = 3456; y = len(heave)-1
    plt.xlim(left = 1350,right = 1800)
    plt.ylim(bottom = 0-round(Hmax+0.5)/2,top = round(Hmax+0.5)/2)
    aa = (np.abs(Below-x)).argmin()
    bb = (np.abs(Below-y)).argmin()
##  plt.plot(Times[Below[aa-1:]],heave[Below[aa-1:]],linewidth=0,c='xkcd:orangered',marker='o',markersize=4)
    if spikes.size:
        bb1 = spikes[np.nonzero((spikes>= x)&(spikes< y))]
        if bb1.size:
            for __ in bb1:
                plt.plot(Times[__],heave[__],linewidth=0,marker='o',markeredgecolor='r',markerfacecolor='none',markersize=10)
    plt.plot(Times[x-5:y],heave[x-5:y],linewidth=0.75,marker='x',markersize=1,c='xkcd:azure')
    plt.fill_between(Times[x-5:y], 0, heave[x-5:y], facecolor='lightblue')
    plt.plot([Times[x-5],Times[y]],[0.05,0.05],linewidth=0.25,c='xkcd:aquamarine')
    plt.plot([Times[x-5],Times[y]],[-0.05,-0.05],linewidth=0.25,c='xkcd:aquamarine')
    plt.tick_params(axis='both',which='major',labelsize=10)
    plt.grid(True,linewidth=0.5)
    
    plt.show()
    
    return()   # Plot_heave()
	
def Plot_heave_north_west(heave,north,west,Hsig,Hmax,Tz,Tp,Record_time,spikes):
##########################################
    """
    plot time-series of current half-hour, displaying Heave, displacement North, and displacement West
    """
    plt.figure(figsize=(20,15),dpi=100)
    plt.subplot(3, 1, 1)
    heave = np.array(heave)
    spikes = np.where(np.abs(heave) > np.median((heave)+3.5*np.std(heave)))    # NOTE - used Median instead of Mean (as median is more resistant to outliers)
    Times = np.linspace(0,1800,num=4608) # Populate an array with 4608 points at 1/2.56 spacing (i.e. 1800s)
    heave_spike_locations = np.array(Times[spikes[:][0]])
    np.set_printoptions(precision=2)
    spikes = spikes[0][:]
    if(len(heave_spike_locations) > 0): 
        print('ALERT: Possible spikes in the Heave data at times:',heave_spike_locations)
        heave = np.delete(heave,spikes[:][0])
    if(len(heave)<len(Times)): 
        diff = len(Times) - len(heave)
        Times = Times[0:len(heave)]    # Identified an error in heave, so chop end off Times
        print('ALERT: ',diff,' Heave values missing from file!')
    
    plt.title(Record_time,fontsize=10)
    plt.xlabel('Heave displacement')
    plt.xlim(left = 0,right = 1800)
#    plt.ylim(bottom = 0-round(Hmax+0.5)/2,top = round(Hmax+0.5)/2)
#    plt.ylim(bottom = 0-round(max(np.abs(heave))),top = round(max(np.abs(heave))))
    plt.ylim(bottom = 0-round(max(max(heave),abs(min(heave)))+0.5),top = round(max(max(heave),abs(min(heave)))+0.5))
    plt.tick_params(axis='both', which='major', labelsize=10)
    plt.plot(Times,heave,linewidth=0.5)
    if spikes.size:
        for __ in spikes:
                plt.plot(Times[__],heave[__],linewidth=0,marker='o',markeredgecolor='r',markerfacecolor='none',markersize=5)
    plt.plot([Times[0],Times[-1]],[0.05,0.05],linewidth=0.25,c='xkcd:aquamarine')
    plt.plot([Times[0],Times[-1]],[-0.05,-0.05],linewidth=0.25,c='xkcd:aquamarine')
    plt.text(1, max(heave)*0.8, '  Hsig = '+"%.2f" % Hsig+'m'+'    Hmax = '+"%.2f" % Hmax+'m'+\
             '    Tz = '+"%.2f" % Tz+'s'+'    Tp = '+"%.2f" % Tp+'s')
    plt.grid(True,linewidth=0.5)  
    
    plt.subplot(3, 1, 2)
    spikes = np.where(np.abs(north) > np.median((north)+3.5*np.std(north)))    # NOTE - used Median instead of Mean (as median is more resistant to outliers)
    north_spike_locations = np.array(Times[spikes[:][0]])
    np.set_printoptions(precision=2)
    spikes = spikes[0][:]
    if (len(north_spike_locations) > 0): 
        print('ALERT: Possible spikes in the North data at times:',north_spike_locations)
        north = np.delete(north,spikes[:][0])
    north = np.array(north)
    Times = np.linspace(0,1800,num=4608) # Populate an array with 4608 points at 1/2.56 spacing (i.e. 1800s)
    if(len(north)<len(Times)): 
        diff = len(Times) - len(north)
        Times = Times[0:len(north)]    # Identified an error in heave, so chop end off Times
        print('ALERT: ',diff,' North values missing from file!')

#    plt.title(Record_time,fontsize=15)
    plt.xlabel('North displacement')
    plt.xlim(left = 0,right = 1800)
    plt.ylim(bottom = 0-round(max(max(north),abs(min(north)))+0.5),top = round(max(max(north),abs(min(north)))+0.5))
    plt.tick_params(axis='both', which='major', labelsize=10)
    plt.plot(Times,north,linewidth=0.5,c='xkcd:tan')
    if spikes.size:
        for __ in spikes:
                plt.plot(Times[__],north[__],linewidth=0,marker='o',markeredgecolor='r',markerfacecolor='none',markersize=5)
    plt.plot([Times[0],Times[-1]],[0.05,0.05],linewidth=0.25,c='xkcd:aquamarine')
    plt.plot([Times[0],Times[-1]],[-0.05,-0.05],linewidth=0.25,c='xkcd:aquamarine')
    plt.grid(True,linewidth=0.5)

    plt.subplot(3, 1, 3)
    spikes = np.where(np.abs(west) > np.median((west)+3.5*np.std(west)))    # NOTE - used Median instead of Mean (as median is more resistant to outliers)
    west_spike_locations = np.array(Times[spikes[:][0]])
    np.set_printoptions(precision=2)
    spikes = spikes[0][:]
    if (len(west_spike_locations) > 0): 
        print('ALERT: Possible spikes in the West data at times:',west_spike_locations)
        west = np.delete(west,spikes[:][0])
    west = np.array(west)
    Times = np.linspace(0,1800,num=4608) # Populate an array with 4608 points at 1/2.56 spacing (i.e. 1800s)
    if(len(west)<len(Times)): 
        diff = len(Times) - len(west)
        Times = Times[0:len(west)]    # Identified an error in heave, so chop end off Times
        print('ALERT: ',diff,' West values missing from file!')

#    plt.title(Record_time,fontsize=15)
    plt.xlabel('West displacement')
    plt.xlim(left = 0,right = 1800)
    plt.ylim(bottom = 0-round(max(max(west),abs(min(west)))+0.5),top = round(max(max(west),abs(min(west)))+0.5))
    plt.tick_params(axis='both', which='major', labelsize=10)
    plt.plot(Times,west,linewidth=0.5,c='xkcd:salmon')
    if spikes.size:
        for __ in spikes:
                plt.plot(Times[__],west[__],linewidth=0,marker='o',markeredgecolor='r',markerfacecolor='none',markersize=5)
    plt.plot([Times[0],Times[-1]],[0.05,0.05],linewidth=0.25,c='xkcd:aquamarine')
    plt.plot([Times[0],Times[-1]],[-0.05,-0.05],linewidth=0.25,c='xkcd:aquamarine')
    plt.grid(True,linewidth=0.5)
   
    plt.show()
    
    return()    # Plot_heave_north_west()
	
def is_outlier(points, thresh=3.5):
##########################################
    """
    JW note: see https://stackoverflow.com/questions/22354094/pythonic-way-of-detecting-outliers-in-one-dimensional-observation-data    
    
    Returns a boolean array with True if points are outliers and False 
    otherwise.

    Parameters:
    -----------
        points : An numobservations by numdimensions array of observations
        thresh : The modified z-score to use as a threshold. Observations with
            a modified z-score (based on the median absolute deviation) greater
            than this value will be classified as outliers.
    Returns:
    --------
        mask : A numobservations-length boolean array.

    References:
    ----------
        Boris Iglewicz and David Hoaglin (1993), "Volume 16: How to Detect and
        Handle Outliers", The ASQC Basic References in Quality Control:
        Statistical Techniques, Edward F. Mykytka, Ph.D., Editor. 
    """
    if len(points.shape) == 1:
        points = points[:,None]
    median = np.median(points, axis=0)
    diff = np.sum((points - median)**2, axis=-1)
    diff = np.sqrt(diff)
    med_abs_deviation = np.median(diff)

    modified_z_score = 0.6745 * diff / med_abs_deviation

    return modified_z_score > thresh
    
import plotly.graph_objects as go
import numpy as np
import matplotlib.pyplot as plt
    
def Do_XYZ_plot(Heave,North,West,Start_time,Sample_frequency):  
##########################################
    """
    Process Heave
    """
    dt = 1/Sample_frequency
    j = len(Heave)
    Times = np.linspace(0,j*dt,num=j) 
    x1 = np.linspace(0,j*dt,num=j)

    y1 = np.asarray(Heave)
    filtered_x1 = x1[~is_outlier(y1)]
    filtered_y1 = y1[~is_outlier(y1)]

    bad_x1 = x1[is_outlier(y1)]
    bad_y1 = y1[is_outlier(y1)]

# Process North
    dt = 1/Sample_frequency
    j = len(North)
    Times = np.linspace(0,j*dt,num=j) 
    x2 = np.linspace(0,j*dt,num=j)

    y2 = np.asarray(North)
    filtered_x2 = x2[~is_outlier(y2)]
    filtered_y2 = y2[~is_outlier(y2)]

    bad_x2 = x2[is_outlier(y2)]
    bad_y2 = y2[is_outlier(y2)]

# Process West
    dt = 1/Sample_frequency
    j = len(West)
    Times = np.linspace(0,j*dt,num=j) 
    x3 = np.linspace(0,j*dt,num=j)

    y3 = np.asarray(West)
    filtered_x3 = x3[~is_outlier(y3)]
    filtered_y3 = y3[~is_outlier(y3)]

    bad_x3 = x3[is_outlier(y3)]
    bad_y3 = y3[is_outlier(y3)]

    text = ['Custom text {}'.format(i + 1) for i in range(j)]

    heave = go.Scatter(x=x1, y=y1, fill='tozeroy', 
    hovertemplate = '<i>Heave</i>: %{y:.2}m'+ '<br><i>Time</i>: %{x:.2f}s',
                name="Heave", line_color='#99ebff', line_width=2)

    north = go.Scatter(x=x2, y=y2, 
                hovertemplate = '<i>North</i>: %{y:.2}m'+ '<br><i>Time</i>: %{x:.2f}s',
                name="North-South",line_color='salmon', line_width=2)
    west  = go.Scatter(x=x3, y=y3, 
                hovertemplate = '<i>West</i>: %{y:.2}m'+ '<br><i>Time</i>: %{x:.2f}s',
                name="East-West",line_color='wheat', line_width=2)

    fig = go.Figure()

# Plot Heave
    fig.add_trace(heave)
    fig.add_trace(go.Scatter(x=bad_x1, y=bad_y1,mode='markers',marker_color='red', legendgroup="group", showlegend=False, name = 'Suspect'))

# Plot North
    fig.add_trace(north)
    fig.add_trace(go.Scatter(x=bad_x2, y=bad_y2,mode='markers',marker_color='red', legendgroup="group", showlegend=False, name = 'Suspect'))

# Plot West
    fig.add_trace(west)
    fig.add_trace(go.Scatter(x=bad_x3, y=bad_y3,mode='markers',marker_color='red', legendgroup="group", name = 'Suspect'))

    fig.update_xaxes(title_text='Elapsed seconds from start of record (s)')
    fig.update_yaxes(title_text='Displacements (m)')
    fig.update_layout(legend_xanchor='auto', legend_yanchor='top',font_size=15,font_color="black",legend_bgcolor='rgba(0,0,0,0)')

# Place file name as title in top centre of plot
    fig.update_layout(template="plotly_white", title=go.layout.Title(text=str(Start_time)), xaxis_zeroline=True)

    fig.show()
    
    return()    # Do_XYZ_plot()	
	
def Do_Plot_Direction(Record_time,Direction,Spread):
##########################################
    Direction = np.array(Direction)
    Spread = np.array(Spread)
    
    plt.figure(figsize=(20, 8))
    plt.title(Record_time,fontsize=14)

    plt.ylabel('Direction',fontsize=16)
    plt.tick_params(axis='both', which='major', labelsize=15)
    plt.yticks(np.arange(0, 360, 45))
    plt.grid(True)

    plt.plot(Direction+Spread,linewidth=2,color='xkcd:aquamarine')
    plt.plot(Direction,linewidth=10,color='xkcd:lightblue')
    plt.plot(Direction,color='b')
    plt.plot(Direction-Spread,linewidth=2,color='xkcd:azure')

    plt.plot(Direction+Spread-360,color='xkcd:aquamarine',dashes=[5, 5, 5, 5])
    plt.plot(Direction-360,color='b',dashes=[5, 5, 5, 5])
    plt.plot(Direction-360-Spread,color='xkcd:azure',dashes=[5, 5, 5, 5])
    
    plt.plot(Direction+Spread+360,color='xkcd:aquamarine',dashes=[5, 5, 5, 5])
    plt.plot(Direction+360,color='b',dashes=[5, 5, 5, 5])
    plt.plot(Direction+360-Spread,color='xkcd:azure',dashes=[5, 5, 5, 5])
    plt.xlim(left=0,right=65)
    plt.ylim(bottom=360,top=0)
    plt.show()
    
    return()    # Do_Plot_Direction()	
	
def Do_Histogram(x_val,Record_time):
##########################################
    binwidth = 0.2

# Fit a normal distribution to the data:
    mu, std = norm.fit(x_val)
    spike = np.median(x_val) + std*3.5

# Plot the histogram.
    plt.figure(figsize=(20,10))
    plt.hist(x_val, bins=np.arange(round(min(x_val)),round(max(x_val)) + binwidth,binwidth),\
             density=1,edgecolor='black',facecolor='xkcd:aquamarine',alpha=0.5)

# Plot the PDF.
    xmin, xmax = plt.xlim()
    ymin, ymax = plt.ylim()
    x = np.linspace(xmin, xmax, 100)
    p = norm.pdf(x, mu, std)
    plt.plot(x, p, color='orange',linewidth=4)
    plt.xlabel('Histogram of Water surface elevations')
    plt.ylabel('Probability')
    plt.title(Record_time,fontsize=10)
    
    plt.text(spike,0.225, r'3.5$\sigma$',fontsize=20,horizontalalignment='center')
    plt.plot([spike,spike],[0.0,0.2],color="black",dashes=[5, 5, 5, 5])
    plt.text(-spike,0.225, r'-3.5$\sigma$',fontsize=20,horizontalalignment='center')
    plt.plot([-spike,-spike],[0.0,0.2],color="black",dashes=[5, 5, 5, 5])
    
    pos_spikes = sum(x_val>spike)
    if pos_spikes > 0: 
        if pos_spikes == 1:
            spike_txt = ' spike '
        else:
            spike_txt = ' spikes '
        plt.text(spike,ymax*0.8,"%3d" % pos_spikes+spike_txt+'> 3.5$\sigma$',horizontalalignment='center',fontsize=10)
    neg_spikes = sum(x_val<-spike)
    if neg_spikes > 0:
        if neg_spikes == 1:
            spike_txt = ' spike '
        else:
            spike_txt = ' spikes '
        plt.text(-spike,ymax*0.8,"%3d" % neg_spikes+spike_txt+'< -3.5$\sigma$',horizontalalignment='center',fontsize=10)
# Show Skewness and Kurtosis - refer Goda p.298 (9.129) and (9.130) plus accompanying discussion 9.5.1.
    plt.text(xmin*0.95, ymax*0.75,'Skewness = '+"%.2f" % skew(x_val),fontsize=12)
    plt.text(xmin*0.95, ymax*0.70,'Kurtosis = '+"%.2f" % kurtosis(x_val,fisher=False),fontsize=12)
   
    plt.show()
    
    return()    # Do_Histogram() 
	
def Do_Rayleigh(waves,Record_time,Hsig,Hrms,Hmax):
##########################################
    import collections
    plt.figure(figsize=(20,10))
    mean, var, skew, kurt = rayleigh.stats(moments='mvsk')
    x = np.linspace(rayleigh.ppf(0.01),rayleigh.ppf(0.99),100)

# Plot histogram of wave heights
    i=0;nbins = []
    while i<Hmax+0.2:
        nbins.append(i)
        i += 0.2
    i+= 0.2 
    inds = np.digitize(waves,nbins)
    c = collections.Counter(inds)
    a = sorted(c.items())
    bin_count = []
    i = 0; j = 0
    while i <=len(nbins)-2:
        if a[:][j][0]-1 == i:
            bin_count.append(a[:][j][1])
            j += 1
        else:
            bin_count.append(0)
        i+=1
    
    plt.xaxis = (0,max(nbins))
    plt.hist(waves,bins=nbins,density='normed',range=(0,max(waves)),edgecolor='black',facecolor='xkcd:aquamarine',alpha=0.5)
    for i in range(len(nbins)-1):
        plt.text(nbins[i]+0.075,0.05,bin_count[i],fontsize=14)
        
# Plot Rayleigh distribution
##    plt.plot(x, rayleigh.pdf(x,0,1),'r-', lw=4, alpha=0.6,label='Rayleigh')
  
# Do a Rayleigh distribution based on Hrms  
    x1 = np.linspace(0,Hmax,100)
    p_h = []
    for i in x1:
        p_h.append(2*i/Hrms**2*np.exp(-(i/Hrms)**2)) # from Massel p192 (6.43) and Karimpour p.25 (3.13)
    plt.text(nbins[len(nbins)-4], max(p_h)*0.8, 'Hmax = '+"%.2f" % Hmax+'m',horizontalalignment='left',verticalalignment='center',fontsize=15)
    plt.text(nbins[len(nbins)-4], max(p_h)*0.75, 'No. of waves = '+"%.2d" % len(waves),horizontalalignment='left',verticalalignment='center',fontsize=15)

    plt.plot(x1,p_h, lw=4,label='Rayleigh (Hrms)')

# Plot Normal distribution
    y_pdf=sp.stats.norm.pdf(x,np.mean(waves),np.std(waves))
    plt.plot(x,y_pdf, lw=4,label='Normal distribution')
    
    plt.xlabel('Wave height (m)')
    plt.ylabel('Probability')
    plt.legend(frameon=False)
    plt.title(Record_time,fontsize=10)

# Display wave parameter representations on Rayleigh distribution     
    plt.text(Hsig,0.225, r'Hsig',fontsize=20,horizontalalignment='center')
    plt.plot([Hsig,Hsig],[0.0,0.2],color="black",dashes=[5, 5, 5, 5])
    plt.text(Hrms,0.225, r'Hrms',fontsize=20,horizontalalignment='center')
    plt.plot([Hrms,Hrms],[0.0,0.2],color="black",dashes=[5, 5, 5, 5])
    plt.text(Hmax,0.225, r'Hmax',fontsize=20,horizontalalignment='center')
    plt.plot([Hmax,Hmax],[0.0,0.2],color="black",dashes=[5, 5, 5, 5])
    
    plt.show()

    return()    # Do_Rayleigh()
	
def Plot_velocities(heave):
##########################################

    velocities = [(x - heave[i - 1])*2.56 for i, x in enumerate(heave)][1:]
    limit = np.median((velocities)+3.5*np.std(velocities))
    offset = max(abs(min(velocities)),max(velocities))
    vector =([np.sqrt(heave[i]**2+velocities[i]**2) for i in range(len(velocities))])

    plt.figure(figsize=(10,10),dpi=100)   
    plt.xlabel("WSE's (m)")
    plt.ylabel('Velocity (m/s)')
    plt.title("WSE's v Velocity",fontsize=10)
    plt.xlim(left=-offset*1.05,right=offset*1.05)
    plt.ylim(bottom=-offset*1.05,top=offset*1.05)
    plt.grid(True)

    color = ['red' if x >= limit or x<=-limit else 'lightgreen' for x in velocities]

    heave_1 = heave[1:]
    plt.scatter(heave_1,velocities,marker='o',c=color,s=9)
    plt.show()   
    
    return()    # Plot_velocities()
	
###    3d Plot    ###
def set_aspect_equal_3d(ax):
    """
    Fix equal aspect bug for 3D plots.
    """

    xlim = ax.get_xlim3d()
    ylim = ax.get_ylim3d()
    zlim = ax.get_zlim3d()

    from numpy import mean
    xmean = mean(xlim)
    ymean = mean(ylim)
    zmean = mean(zlim)

    plot_radius = max([abs(lim - mean_)
                       for lims, mean_ in ((xlim, xmean),
                                           (ylim, ymean),
                                           (zlim, zmean))
                       for lim in lims])

    ax.set_xlim3d([xmean - plot_radius, xmean + plot_radius])
    ax.set_ylim3d([ymean - plot_radius, ymean + plot_radius])
    ax.set_zlim3d([zmean - plot_radius, zmean + plot_radius])

'''
def Do_3d (heave,north,west,Record_time):
    mpl.rcParams['legend.fontsize'] = 10

    mu_x, std_x = norm.fit(west)
    spike_x = np.median(west) + std_x*3.5

    mu_y, std_y = norm.fit(north)
    spike_y = np.median(north) + std_y*3.5

    mu_z, std_z = norm.fit(heave)
    spike_z = np.median(heave) + std_z*3.5

    fig = plt.figure()
    fig=plt.figure(figsize=(15,15))
    ax = fig.gca(projection='3d')

    ax.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
    ax.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
    ax.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))

    theta = np.linspace(-4 * np.pi, 4 * np.pi, 100)
    x = west
    y = north
    z = heave

    color = ['red' if x <= 0 else 'green' for x in z]

    ax.plot(x, y, z, linewidth=0.2,c='xkcd:cement',label=Record_time)
    ax.scatter(x, y, z, c=color, marker='o',s=5)   

    ax.legend()
    ax.set_xlabel('East-West Displacement (m)')
    ax.set_ylabel('North-South Displacement (m)')
    ax.set_zlabel('Heave Displacement (m)')

    ax.set_aspect('equal') 

    set_aspect_equal_3d(ax)
    plt.show()
    
    return()    # 3d_Plot
'''
	
import plotly.graph_objects as go
import pandas as pd
import numpy as np

def SetColorLine(Heave):
##########################################
    if(Heave < 0):
        return "salmon"
    elif(Heave > 0):
        return "lightgreen"
    elif(Heave == 0):
        return "yellow"
def SetColorPoint(Heave):
    if(Heave < 0):
        return "red"
    elif(Heave > 0):
        return "green"
    elif(Heave == 0):
        return "yellow"

def Do_3d (heave,north,west,Record_time,Sample_frequency):
##########################################
    
    dt = 1/Sample_frequency
    j = len(heave)
    Time = np.arange(0, j, dt)
    
    # create a horizontal plane passing through the origin of the axes
    height=0
    x = np.linspace(-2,2, 10)
    y = np.linspace(-2,2, 10)
    x,y = np.meshgrid(x,y)
    z = np.zeros(x.shape)
    colorscale = "Viridis"

    horz_surf = go.Surface(x=x, y=y, z=z, colorscale=colorscale, showscale=False, hoverinfo='none', opacity=0.5)

    # create a horizontal plane passing through the origin of the axes
    height=0
    x = np.linspace(-2,2, 10)
    y = np.linspace(-2,2, 10)
    x,y = np.meshgrid(x,y)
    z = np.zeros(x.shape)

    horz_surf = go.Surface(x=x, y=y, z=z, colorscale="blues", showscale=False, hoverinfo='none', opacity=0.4)

    # create a vertical plane aligned north-South passing through the origin of the axes and intersecting the East-west axis
    x=np.linspace(-2,2, 10)
    z=np.linspace(-2,2, 10)
    x,z=np.meshgrid(x,y)
    y=np.zeros(x.shape)

    vert_surf = go.Surface(x=x, y=y, z=z, colorscale="greens", showscale=False, hoverinfo='none', opacity=0.4)

    # create a vertical plane aligned north-South passing through the origin of the axes and intersecting the East-west axis
    y=np.linspace(-2,2, 10)
    z=np.linspace(-2,2, 10)
    y,z=np.meshgrid(x,y)
    x=np.zeros(x.shape)

    vert_surf_N = go.Surface(x=x, y=y, z=z, colorscale="reds", showscale=False, hoverinfo='none', opacity=0.4)

    fig = go.Figure(data=[horz_surf,vert_surf,vert_surf_N,go.Scatter3d(x=north, y=west, z=heave,
                text = ['heave: {:.2f}m; north: {:.2f}m; west: {:.2f}m; Time: {:.2f}s'.format(heave[i],north[i],west[i],i/1.28) for i in range(j)], 
                hovertemplate = '<b>%{text}<br>',

                marker=dict(size=3,color=list(map(SetColorPoint,heave))),
                line=dict(color=list(map(SetColorLine,heave)),width=2), opacity=0.75)])

    fig.update_layout(title=str(Record_time),
        width=900,height=900,
        margin=dict(l=5,r=5,b=15,t=25),
        autosize=True,
        scene=dict(
            xaxis=dict(gridcolor='lightgrey',
                zerolinecolor='red',
                zerolinewidth=4,
                showbackground=False),
            yaxis=dict(gridcolor='lightgrey',
                zerolinecolor='green',
                zerolinewidth=4,
                showbackground=False),
            zaxis=dict(gridcolor='lightgrey',
                zerolinecolor='blue',
                zerolinewidth=4,
                showbackground=False),
                camera=dict(up=dict(x=0,y=0,z=.5),
                eye=dict(x=-1.25,y=-1.25,z=1.25)),
            aspectratio = dict( x=1, y=1, z=1 ),aspectmode = 'cube'))

    fig.update_layout(scene = dict(
                        xaxis_title='NORTH - SOUTH (+ve North)',
                        yaxis_title='WEST - EAST (+ve West)',
                        zaxis_title='HEAVE'),
                        width=700,
                        margin=dict(r=20, b=10, l=10, t=10))

    fig.show()
    
    return()    # Do_3d ()
	
def SetColorPoint(t):
##########################################
    if(t < 0):
        return "red"
    elif(t > 0):
        return "green"
    elif(t == 0):
        return "yellow"

def pol2cart(rho, phi):
##########################################
    north = rho * math.cos(math.radians(phi))
    east = rho * math.sin(math.radians(phi))
    return(north, east)
  
def Do_polar(Heave,North,West,Record_time,Sample_frequency):
##########################################
    import plotly as py
    import plotly.graph_objects as go
    import math

    # Process North
    dt = 1/Sample_frequency    # For Mk3 and WRG
    j = len(Heave)

    r = np.sqrt(np.asarray(North, dtype=np.float32)**2+np.asarray(West, dtype=np.float32)**2)
    t = -np.arctan2(np.asarray(West, dtype=np.float32),np.asarray(North, dtype=np.float32))    

    fig = go.Figure(data=
                go.Scatterpolar(
                    r = r,
                    theta = t*180/np.pi,
                    mode = 'lines+markers',
                    text = ['Heave: {:.2f}m; North: {:.2f}m; West: {:.2f}m; Time: {:.2f}s'.format(Heave[i],North[i],West[i],i/1.28) for i in range(j)], 
                    hovertemplate = '<b>%{text}<br>',

                    marker_size=5,marker_color=list(map(SetColorPoint,Heave)),
                    line_color='lightblue',line_width=0.75, opacity=0.95,
          ))

    fig.update_layout(
        template=None,
        polar = dict(
            radialaxis = dict(range=[0, max(r)*1.1], showticklabels=True, tickfont_size = 15,),
            angularaxis = dict(direction="clockwise",rotation=90,showticklabels=True)
        )
        )

    
    fig.update_layout(template="plotly_white",title=str(Record_time),
            width=800,height=800,
            margin_l=5,margin_r=5,margin_b=15,margin_t=45,
            showlegend=False)

    fig.show()
    
    return()    # Do_polar()
	
import plotly.graph_objects as go

def Do_Box_plot(Heave,North,West,Start_time,Sample_frequency):
##########################################
    """
    Do box plots of Heave, North, West to identify suspects
    """

    # Process Heave
    dt = 1/Sample_frequency
    j = len(Heave)
    Times = np.linspace(0,j*dt,num=j) 
    x1 = np.linspace(0,j*dt,num=j)

    text=['Time: '+'{:.2f}'.format(x1[k]) for k in range(len(x1))]

    x_data = ['Heave', 'North', 'West',]
    y_data = [Heave, North, West]

    colors = ['#99ebff', 'salmon', 'wheat']

    fig = go.Figure()

    for xd, yd, cls in zip(x_data, y_data, colors):
            fig.add_trace(go.Box(
                width=1,
                y=yd,
                name=xd,
                text=text,
                boxpoints='suspectedoutliers',
                marker_color='red', 
                marker_outliercolor='lightblue',
                jitter=0.75,
                pointpos=0,
                whiskerwidth=1,
                fillcolor=cls,
                marker_size=7
            ))

    fig.update_layout(
        width=1000,height=800,
        margin=go.layout.Margin(l=40, r=30, t= 100, b=80),
        showlegend=True
    )

    fig.update_yaxes(zeroline=True, zerolinewidth=3, zerolinecolor='grey')
    
    fig.update_layout(
        template="plotly_white", title=go.layout.Title(text=str(Start_time),xref='paper',x=0.5))

    fig.show()
    
    return()    # Do_Box_plot()    	